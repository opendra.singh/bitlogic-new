<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Private_Controller extends Site_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('private');

        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');

        if (!self::check_roles(1)) {
            if(Settings_model::$db_config['disable_all'] == 1) {
                $this->session->set_flashdata('error', '<p>'. $this->lang->line('site_disabled') .'</p>');
                redirect('login');
            }
        }

        $this->load->helper('cookie');
        $cookie_domain = config_item('cookie_domain');

        // no session data but cookie is set
        if ($this->session->userdata('user_id') == "" && get_cookie('unique_token') != "") {

            // check cookie data
            $this->load->model('utils/set_cookies_model');
            $cookie = get_cookie('unique_token');
            $a = substr($cookie, 0, 32);
            $b = substr($cookie, 42, 74);
            $userdata = $this->set_cookies_model->load_session_vars($a, $b);

            if (!empty($userdata)) {
                // check banned and active
                if ($userdata->banned != 0) {
                    $this->session->set_flashdata('error', '<p>You are banned.</p>');
                    setcookie("unique_token", null, time() - 60*60*24*3, '/', $cookie_domain, false, false);
                    redirect("login");
                }elseif($userdata->active != 1) {
                    $this->session->set_flashdata('error', '<p>Your acount is inactive.</p>');
                    setcookie("unique_token", null, time() - 60*60*24*3, '/', $cookie_domain, false, false);
                    redirect("login");
                }

                // renew cookie
                setcookie("unique_token", get_cookie('unique_token'), time() + Settings_model::$db_config['cookie_expires'], '/', $cookie_domain, false, false);

                // set session data
                $this->load->helper('session');
                set_session_data($userdata->user_id, $userdata->username);

                // get permissions
                $this->permissions_roles($userdata->user_id);

                redirect('membership/'. strtolower(Settings_model::$db_config['home_page']));
            }else{
                setcookie("unique_token", null, time() - 60*60*24*3, '/', $cookie_domain, false, false);
                redirect("login");
            }

            // no session data, no cookie data
        }elseif ($this->session->userdata('user_id') == "" && !get_cookie('unique_token')) {
            setcookie("unique_token", null, time() - 60*60*24*3, '/', $cookie_domain, false, false);
            redirect("login");
        }


        // get permissions when session is present, too but then from session in stead of query
        //$this->permissions_roles($this->session->userdata('user_id'));

    }



}
