<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gateway_Controller extends MY_Controller
{
    public static $permissions = array();
    public static $roles = array();
    public static $page;
    private $_meta_description;

    public function __construct()
    {
        parent::__construct();
        // set page name
        self::$page = $this->router->fetch_class();
        $this->lang->load('public');

        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');

    }


    public static function check_roles($role_id) {
        //var_dump($role_id);die();
        foreach (self::$roles as $role) {
            if (is_array($role_id)) {
                if (in_array ($role->role_id, $role_id)) {
                    return true;
                }
            }elseif ($role->role_id == $role_id) {
                return true;
            }
        }
        return false;
    }

    public static function check_permissions($permission_id) {
        foreach (self::$permissions as $permission) {
            if (is_array($permission_id)) {
                if (in_array ($permission->permission_id, $permission_id)) {
                    return true;
                }
            }elseif ($permission->permission_id == $permission_id) {
                return true;
            }
        }
        return false;
    }

    public function permissions_roles($user_id) {
        $this->load->model('utils/rbac_model');
        self::$permissions = $this->rbac_model->get_member_permissions($user_id);
        self::$roles = $this->rbac_model->get_member_roles($user_id);
    }

    public function get_meta_description() {
        return $this->_meta_description;
    }

    public function set_meta_description($description) {
        $this->_meta_description = $description;
    }

}
