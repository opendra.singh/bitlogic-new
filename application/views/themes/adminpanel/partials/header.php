<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <header class="navbar header" role="menu">
        <div class="navbar-header">
            <a class="navbar-brand block" href="<?php print base_url(); ?>">
                <i class="fa fa-cubes pull-left pd-r-10"></i>
                <div class="navbar-brand-title">
                    Bitlogik <span class="navbar-brand-title-small" style="vertical-align:bottom">1.0</span>
                </div>
            </a>
        </div>

        <a id="js-showhide-menu" href="javascript:" class="btn navbar-btn">
            <i class="fa fa-eye-slash"></i>
        </a>

        <a id="js-narrow-menu" href="javascript:" class="btn navbar-btn">
            <i class="fa fa-bars"></i>
        </a>


<div class="btn-group pull-right">
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: uppercase; padding: 14px 4px;">
    <i class="fa fa-sign-in" aria-hidden="true"></i> <?php print $this->session->userdata('first_name'); ?>&nbsp;<?php print $this->session->userdata('last_name'); ?>
  </button>
  <div class="dropdown-menu" style="height:40px">
    <a class="dropdown-item" href="<?php print base_url(); ?>logout" style="padding:8px;font-size:20px" ><i class="fa fa-power-off fg-danger"></i>Logout</a>
  </div>
</div>

    </header>