<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php print Settings_model::$db_config['site_title']; ?>: <?php print $template['title']; ?></title>
  <?php print $template['metadata']; ?>
  <meta name="viewport" content="width=device-width, maximum-scale=1, minimum-scale=1" />
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- stylesheet -->
  <link href="<?php print base_url(); ?>assets/css/<?php print Settings_model::$db_config['active_theme']; ?>/bootstrap.min.css" rel="stylesheet">   
  <link href="<?php print base_url(); ?>assets/css/<?php print Settings_model::$db_config['active_theme']; ?>/style.css" rel="stylesheet">  
  
  <!-- CSS for  side menu -->
  <link rel="stylesheet" href="<?php print base_url(); ?>assets/css/custom/footer.css">
  
  <!-- Font Awesome CSS For ALL -->
  <link rel="stylesheet" href="<?php print base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
  <!-- Slider -->
  <script src="<?php print base_url(); ?>assets/slider/js/jquery-1.12.4.min.js"></script>
  
</head>

<body class="notransitioner<?php print " ". Site_controller::$page; ?>">

  
  <?php print $template['partials']['header']; ?>

  
  <?php print $template['body']; ?>
  

  
  <?php print $template['partials']['footer']; ?>
  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    
    
    <?php print $template['js']; ?>
    <?php $this->load->view('generic/js_system'); ?>
    
    <script src="<?php print base_url(); ?>assets/vendor/parsley/parsley.min.js"></script>

</body>
</html>