<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {

        //value 1
        $numeroa = rand(1, 9);
        //value2
        $numerob = rand(1, 9);
        $numero = $numeroa + $numerob;
        $display = $numeroa . ' + ' . $numerob;
        $_SESSION['check'] = $numero; 
        //The function imagecolorallocate creates a 
        //color using RGB (red,green,blue) format.

        $content["captcha"] = $display;
        $this->quick_page_setup(Settings_model::$db_config['active_theme'], 'main',  $this->lang->line('login'), 'login', 'header', 'footer', '', $content);
    }

    /** validate: validate login after input fields have met requirements  */

    public function validate() {
        
        if(($this->input->post('login_submit')!=NULL && $this->input->post('login_submit') == "Validate Login...") && $this->input->post('identification')!=NULL && $this->input->post('password')!=NULL && $this->input->post('captext')!=NULL)
        {
            if($_SESSION['check'] != $this->input->post('captext'))
            {
                $this->session->set_flashdata('error', "Please enter correct captcha.");
                redirect('login');
            }
            unset($_SESSION['check']);

            $this->form_validation->set_error_delimiters('<p>', '</p>');
            $this->form_validation->set_rules('identification', $this->lang->line('login_identification'), 'trim|required|min_length[5]|max_length[24]|is_valid_username');
            $this->form_validation->set_rules('password', $this->lang->line('login_password'), 'trim|required|min_length[9]|max_length[255]');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('login');
            }

            $this->load->model('auth/login_model');

            // database work
            $data = $this->login_model->validate_login($this->input->post('identification'), $this->input->post('password'), Settings_model::$db_config['allow_login_by_email']);
 
            // check banned
            if ($data == "banned") { 
                $this->session->set_flashdata('error', '<p>'. $this->lang->line('account_access_denied') .'</p>');
                redirect('login');
            } 
            elseif (is_array($data)) {
                // check active
                if ($data['active'] == 0) { 
                    $this->session->set_flashdata('error', '<p>'. $this->lang->line('account_activate') .'</p>');
                    redirect('login');
                }else{

                // user is fine, now load roles and set session data
                $this->permissions_roles($data['user_id']);

                // let administrators through, the other roles will be redirected when checks below match
                if (!self::check_roles(1)) {
                    if(Settings_model::$db_config['login_enabled'] == 0) {
                        $this->session->set_flashdata('error', '<p>'. $this->lang->line('login_disabled') .'</p>');
                        redirect('login');
                    }
                }

                // set session data
                $this->load->helper('session');
                set_session_data($data['user_id'], $data['first_name'], $data['last_name'], $data['username']);
                
                redirect('membership/'. strtolower(Settings_model::$db_config['home_page']));
            }
        }else{
            $this->session->set_flashdata('error', $this->lang->line('login_incorrect'));
            redirect('login');
        }
    }
    else
    {
        redirect('login');
    }
}

}
