<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/' . Settings_model::$db_config['adminpanel_theme'] . '/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
  function submitform() {

      var f = document.getElementsByTagName('form')[0];

      if($("#username").val()=="" && $("#currency").val()=="" && $("#uniqueTransactionId").val()=="" && $("#amount").val()=="")
      { 
        alert("Invalid request. refresh and try again");
        $('#pay_now').val("Pay Now");
        return false;
      }
      else if(f.checkValidity()) {
        f.submit();
      } else {
        $('#pay_now').val("Pay Now");
      }
    }
</script>

<div class="container">
  <h2>Genome Payment Service Test Page</h2>
  <hr/>

  <?php if($username == "" || $currency == "") {?>


    <div class="alert alert-danger" role="alert">
      Username and currency not found.
    </div>

  <?php } else { ?>
  <?php print form_open('genome/genome_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>
  

    <div class="form-group row">
      <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
    </div>

    <div class="form-group row">
      <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
        <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
      </label>
    </div>

    <div class="form-group row">
      <label for="amount">Amount: </label>
      <div class="input-group input-group-sm">
        <div class="input-group-btn">
          <button class="btn btn-default"><?php echo $currency; ?></button>
        </div>
        <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="">
      </div>
    </div>
    
    <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
    <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
    <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
    <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>
    
  <?php print form_close() . "\r\n"; ?> 

    <?php } ?>
</div>
