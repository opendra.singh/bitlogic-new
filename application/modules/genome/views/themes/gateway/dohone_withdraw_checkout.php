<?php
require APPPATH.'third_party/dohone/vendor/mathermann/dohone-sdk/src/DohonePayoutSDK.php';
require APPPATH.'third_party/dohone/vendor/mathermann/dohone-sdk/src/TransactionInterface.php';
use Mathermann\DohoneSDK\DohonePayoutSDK;
use Mathermann\DohoneSDK\TransactionInterface;
use Mathermann\DohoneSDK\InvalidDohoneResponseException;
class Transaction implements TransactionInterface
{
    /**
     * Transaction reference (or id) in your system
     */
    private $transactionRef;
    
    /**
     * Transaction operator, must be one of the following values:
     * ['DOHONE_MOMO', 'DOHONE_OM', 'DOHONE_EU', 'DOHONE_TRANSFER']
     */
    private $transactionOperator = 'DOHONE_TRANSFER';
    
    private $transactionAmount;
    
    /**
     * Transaction currency, must be one of the following values:
     * ['XAF', 'EUR', 'USD']
     */
    private $transactionCurrency;
    
    private $transactionReason = 'user_withdraw';
    
    /**
     * Transaction reference in Dohone's system
     */
    private $dohoneTransactionRef;
    
    private $customerName;
    
    private $customerPhoneNumber;
    
    private $customerEmail;
    
    private $customerCountry;
    
    private $customerCity;
    
    /**
     * Notification URL for this transaction
     */
    private $notifyUrl;
    
    // You can add some additional properties...

    function __construct($uniqueTransactionId, $amount, $currency, $name, $mobile, $email, $country, $city, $notify_url) {
        $this->transactionRef = $uniqueTransactionId;
        $this->transactionAmount = $amount;
        $this->transactionCurrency = $currency;
        $this->customerName = $name;
        $this->customerPhoneNumber = $mobile;
        $this->customerEmail = $email;
        $this->customerCountry = $country;
        $this->customerCity = $city;
        $this->notifyUrl = $notify_url;
    }
    
    
    public function getTransactionRef()
    {
        return $this->transactionRef;
    }

    public function getTransactionOperator()
    {
        return $this->transactionOperator;
    }

    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    public function getTransactionCurrency()
    {
        return $this->transactionCurrency;
    }

    public function getTransactionReason()
    {
        return $this->transactionReason;
    }

    public function getDohoneTransactionRef()
    {
        return $this->dohoneTransactionRef;
    }

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function getCustomerPhoneNumber()
    {
        return $this->customerPhoneNumber;
    }

    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    public function getCustomerCountry()
    {
        return $this->customerCountry;
    }

    public function getCustomerCity()
    {
        return $this->customerCity;
    }

    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }
}

define('ACCOUNT', $mobile);
define('HASH_CODE', $hash_uniq_code);
define('NOTIFY_URL', $notify_url);
$dohonePayoutSdk = new DohonePayoutSDK(ACCOUNT, HASH_CODE, NOTIFY_URL);
$transaction = new Transaction($uniqueTransactionId, $amount, $currency, $name, $mobile, $email, $country, $city, $notify_url);
?>
    <div class="row">
        <div class="panel-footer" style="background-color:#222d32;height:40px;">
            <div class="row">
                <div class="box-header" style="width:100%;">
                    <div class="col-md-12">
                        <p style="text-align: right; color:#fff;padding-top: 9px;">Bitlogik Deposit Payment Service</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p><strong>Amount</strong> : <?php echo $amount; ?></p>
    <p><span><strong>Currency</strong> : <?php echo $currency; ?></span></p>
    <p><span><strong>Transaction Id (Note it down for future references )</strong> : <?php echo $uniqueTransactionId; ?></span></p>
    <p><span><strong>Name</strong> : <?php echo $name; ?></span></p>
    <p><span><strong>Phone Number</strong> : <?php echo $mobile; ?></span></p>
    <p><span><strong>Email</strong> : <?php echo $email; ?></span></p>
    <p><span><strong>Country</strong> : <?php echo $country; ?></span></p>
    <p><span><strong>City</strong> : <?php echo $city; ?></span></p>
<?php
try 
{
    /**
     * $transaction is an object of type Transaction defined above
     */
    $response = $dohonePayoutSdk->quote($transaction);
    
    if ($response->isSuccess())
    {
        //echo $response->getMessage(); // display result
    }
    else
    {
        echo $response->getMessage().'<br/>'; // display error message
    }                
}
catch (InvalidDohoneResponseException $e)
{
    echo $e->getMessage(); // display error message
}

try
{
    /**
     * $transaction is an object of type Transaction defined above
     */
    $response = $dohonePayoutSdk->transfer($transaction);
    
    if ($response->isSuccess() && $response->hasREF()) {
        echo "We have received your request, it will be approved after reviewing the BitLogic Team.";
    }
    else {
        echo $response->getMessage(); // display error message
    }
}
catch (InvalidDohoneResponseException $e)
{
    echo $e->getMessage(); // display error message
}
?>
<div class="row wrap header-fixed">
    <div class="panel-footer" style="background-color:#222d32;height:40px;width:100%;bottom:0;position: inherit;margin-top:50px;"> 
        <div class="row">
            <div class="box-header" style="width:100%;">
                <div class="col-md-12">
                    <p style="text-align: center;color:#fff;padding-top: 9px; ">Designed &amp; Developed By Dezzex Technology Pvt. Ltd. </p>
                </div>
            </div>
        </div>
    </div>
</div>