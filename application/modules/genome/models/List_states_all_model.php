<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class List_states_all_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->table_district = "district_master";
    }

   

   public function get_district() {

        $this->db->select('*')->from(DB_PREFIX .$this->table_district);
        $q = $this->db->get();
        if($q->num_rows() > 0) {

            return $q->result_array();
        }
        return false;
   
   }

}
