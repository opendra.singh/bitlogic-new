<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Family_model extends CI_Model {

    public function __construct() {
        parent::__construct();

    }

     /*Code for Anganbari*/
    public function get_anganbari($args, $awccode=NULL) {
        $table = 'awc_master';
        $columns = 'code, name';
        $where = array('sectorcode'=>$args, 'active'=>1);
        if(isset($awccode))
            $where = array('code'=>$awccode, 'active'=>1);
        $orderBy = array('name'=>'asc');
        return $this->sql_helper->getArray($table, $columns, $where, $orderBy);
    }

    public function get_awc($code) {
        $table = 'awc_master';
        $columns = 'code,name,distcode,projectcode,sectorcode';
        $where = array('code'=>$code);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_awclist($where) {
        $table = 'awc_master';
        $columns = 'code,name,distcode,projectcode,sectorcode';
        $order = array('name'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $order);
    }

    public function list_family($awccode, $distcode, $projectcode, $sectorcode) {
        $table='family_master';
        $column='id,code,auto_code,name,gender,dob,is_head,active,distcode,projectcode,sectorcode,awccode';
        $where=array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode,'active'=>1);
        $order_by= array("code"=>"asc");
        return $this->sql_helper->getResult($table, $column, $where, $order_by);
    }

    public function grp_familylist($awccode, $distcode, $projectcode, $sectorcode) {
        $table='family_master';
        $column='id,code,auto_code,name,gender,dob,is_head,active,distcode,projectcode,sectorcode,awccode';
        $where=array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode,'active'=>1);
        $groupby = array("auto_code");
        $order_by= array("code"=>"asc");
        return $this->sql_helper->getResult($table, $column, $where, $order_by, $groupby);
    }


    public function get_totalmember($target_code, $distcode, $projectcode, $sectorcode, $awccode) {
        $table='family_master';
        $where=array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode,'target_code'=>$target_code);
        return $this->sql_helper->getCount($table, $where);
    }


    public function verify_familylist($awccode, $distcode, $projectcode, $sectorcode, $target_src=NULL) {
        $table='family_master';
        $column='id,code,auto_code,is_residence,category_code,name,gender,dob,marital_status,adhar_no,disability_type,mobile_no,is_head,target_code,active,distcode,projectcode,sectorcode,awccode';
        
        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        if(isset($target_src))
            $where["target_code"] = $target_src;
        else
            $where["target_code !="] = 'N';

        $where["isNew"] = NULL;

        $order_by= array("code"=>"asc");
        return $this->sql_helper->getResult($table, $column, $where, $order_by);
    }

    public function get_maritalstatus() {
        $table = 'maritalstatus_master';
        $columns = 'marital_status';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_disabilitytype() {
        $table = 'disability_master';
        $columns = 'disability_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_beneficiarytype($data=NULL) {
        $table = 'beneficiarytype_master';
        $columns = 'id, name, service_age, service_period, expiry_age, expiry_period, target_code';

        $where = array('active'=>1);
        if(isset($data))
            $where = $data;
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_memberdata($id, $distcode, $projectcode, $sectorcode, $awccode, $autocode) {
        $table = 'family_master';
        $columns = 'id,code,auto_code,is_residence,category_code,name,hindi_name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,target_code,father_id,mother_id,relation,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        if(isset($autocode))
            $where["auto_code"] = $autocode;

        $where["id"] = $id;

        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_othermember($id, $autocode, $gender, $distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'family_master';
        $columns = 'id,code,name';

        $where = array();
        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        if(isset($autocode))
            if($autocode>0)
                $where["auto_code"] = $autocode;

        if($id != NULL)
            $where["id!="] = $id;

        if($gender != NULL)
            $where["gender"] = $gender;

        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_memberresult($autocode, $is_head, $distcode, $projectcode, $sectorcode, $awccode=NULL) {
        $table = 'family_master';
        $columns = 'id,code,auto_code,is_residence,category_code,name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,weight,target_code,height,muac,father_id,mother_id,death_status,death_date,death_remarks,child_sequence,relation,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["auto_code"] = $autocode;

        if(isset($is_head) && $is_head!=NULL)
            $where["is_head"] = $is_head;

        $order_by = array("dob"=>"asc");

        return $this->sql_helper->getResult($table, $columns, $where, $order_by);
    }


    public function get_memberresultbycode($distcode, $projectcode, $sectorcode, $awccode, $code)
    {
        $table = 'family_master';
        $columns = 'id,code,auto_code,is_residence,category_code,name,hindi_name,gender,dob,is_head,mobile_no,target_code,father_id,mother_id,relation,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["auto_code"] = $code;

        $order_by = array("dob"=>"asc");

        return $this->sql_helper->getResult($table, $columns, $where, $order_by);
    }

    public function get_memberarray($autocode, $distcode, $projectcode, $sectorcode, $awccode=NULL) {
        $table = 'family_master';
        $columns = 'id,code,auto_code,is_residence,category_code,name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,weight,target_code,height,muac,father_id,mother_id,death_status,death_date,death_remarks,child_sequence,relation,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["auto_code"] = $autocode;

        return $this->sql_helper->getArray($table, $columns, $where);
    }

    public function get_anccheckupdata($member_id, $family_code, $distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'womenanccheckup_details ';
        $columns = 'id,womenstatus_id,vhnd_date,vmonth,vyear,member_id,family_code,thr,anc1delivery,anc2delivery,anc3delivery,anc4delivery,ifa_count,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        $where["awccode"] = $awccode;
        $where["family_code"] = $family_code;
        $where["member_id"] = $member_id;
        $where["active"] = 1;

        $orderby = array("id"=>"desc");
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }

    public function family_alter($family_id, $family_ac, $family_code, $member_name, $hindi_name, $is_head, $mobile_no, $gender, $dob, $marital_status, $adhar_no, $disability_type, $select_bentype, $father_id, $relation, $mother_id, $active, $distcode, $projcode, $sectcode, $awccode, $loggeduser)
    {
        $table="family_master";

        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projcode,
        'sectorcode' => $sectcode,
        'awccode' => $awccode,
        'auto_code' => $family_ac,
        'code' => $family_code,
        'id' => $family_id
        );

        if($this->sql_helper->getCount($table, $whereArray)>0)
        {
            $cnt =$this->sql_helper->retText($table, 'total_updation', $whereArray)+1;

            $father_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $father_id));
            if(!isset($father_name))
                $father_name = "";

            $mother_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $mother_id));
            if(!isset($mother_name))
                $mother_name = "";

            $columnArray = array(
            'name' => strtoupper($member_name),
            'hindi_name' => strtoupper($hindi_name),
            'gender' => strtoupper($gender),
            'dob' => $dob,
            'marital_status' => strtoupper($marital_status),
            'is_head' => strtoupper($is_head),
            'adhar_no' => $adhar_no,
            'disability_type' => strtoupper($disability_type),
            'mobile_no' => $mobile_no,
            'target_code' => strtoupper($select_bentype),
            'father_id' => $father_id,
            'fh_name' => strtoupper($father_name), 
            'relation' => $relation,
            'mother_id' => $mother_id,
            'mother_name' => strtoupper($mother_name),
            'last_update_by' => $loggeduser,
            'total_updation' => $cnt,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'active' => $active
            );

            if($adhar_no == "*")
            {
                $columnArray = array(
                'name' => strtoupper($member_name),
                'hindi_name' => strtoupper($hindi_name),
                'gender' => strtoupper($gender),
                'dob' => $dob,
                'marital_status' => strtoupper($marital_status),
                'is_head' => strtoupper($is_head),
                'disability_type' => strtoupper($disability_type),
                'mobile_no' => $mobile_no,
                'target_code' => strtoupper($select_bentype),
                'father_id' => $father_id,
                'fh_name' => strtoupper($father_name), 
                'relation' => $relation,
                'mother_id' => $mother_id,
                'mother_name' => strtoupper($mother_name),
                'last_update_by' => $loggeduser,
                'total_updation' => $cnt,
                'last_update_date' => date('Y-m-d'),
                'ipaddress' => $_SERVER["REMOTE_ADDR"],
                'active' => $active
                );
            }

            $this->sql_helper->update_data($table, $columnArray, $whereArray);
        }
    }

    public function familymember_add($family_code, $family_ac, $is_residence, $category_code, $member_name, $is_head, $mobile_no, $gender, $dob, $marital_status, $adhar_no, $disability_type, $select_bentype, $father_id, $relation, $mother_id, $isactive, $awccode, $distcode, $projcode, $sectcode, $loggeduser)
    {

        
        $table="family_master";
        $father_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $father_id));
        if(!isset($father_name))
            $father_name = "";

        $mother_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $mother_id));
        if(!isset($mother_name))
            $mother_name = "";

        $columnArray = array(
            'code' => $family_code,
            'auto_code' => $family_ac,
            'is_residence' => $is_residence,
            'category_code' => $category_code,
            'name' => strtoupper($member_name),
            'gender' => strtoupper($gender),
            'dob' => $dob,
            'marital_status' => strtoupper($marital_status),
            'is_head' => strtoupper($is_head),
            'adhar_no' => strtoupper($adhar_no),
            'disability_type' => strtoupper($disability_type),
            'mobile_no' => $mobile_no,
            'target_code' => strtoupper($select_bentype),
            'distcode' => $distcode,
            'projectcode' => $projcode,
            'sectorcode' => $sectcode,
            'awccode' => $awccode,
            'entry_date' => date("Y-m-d"),
            'entry_by' => $loggeduser,
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'active' => $isactive,
            'father_id' => $father_id,
            'fh_name' => strtoupper($father_name), 
            'relation' => $relation,
            'mother_id' => $mother_id,
            'mother_name' => strtoupper($mother_name),
            'weight' => 0,
            'height' => 0,
            'total_updation' => 0,
            'last_update_by' => $loggeduser,
            'last_update_date' => date('Y-m-d'),
            'death_status' => 0,
            'isNew' => 1
        );

        return $this->sql_helper->insert_data($table, $columnArray);
    }

    public function familymaster_update($regcode, $is_residence, $category_code, $autocode, $awccode, $distcode, $projcode, $sectcode)
    {
        $table="family_master";

        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projcode,
        'sectorcode' => $sectcode,
        'awccode' => $awccode,
        'auto_code' => $autocode
        );

        $columnArray = array(
            'code' => $regcode,
            'is_residence' => $is_residence,
            'category_code' => $category_code,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
        );

        return $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }

    public function get_womenlist($distcode, $projectcode, $sectorcode, $awccode)
    {
        $table = 'family_master';
        $columns = 'id, code, auto_code, name, dob, marital_status, target_code, distcode, projectcode, sectorcode, awccode, active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        $where["awccode"] = $awccode;
        $where["gender"] = 'F';
        $where["active"] = 1;

        $orderby = array("name"=>"asc");
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }

    public function update_wtarget($family_id, $family_code, $targetcode, $lmp, $edd, $registration_date, $mctsno, $anc1due, $anc2due, $anc3due, $anc4due, $delv, $distcode, $projcode, $sectcode, $awccode, $bl=0, $bd=0, $gl=0, $gd=0, $delivtype=NULL, $loggeduser)
    {
        $table = 'womenstatus_master';

        if($targetcode == "L")
        {
            $lmp = NULL;
            $edd = NULL;
            $registration_date = NULL;
            $mctsno = NULL;
            $anc1due = NULL;
            $anc2due = NULL;
            $anc3due = NULL;
            $anc4due = NULL;
        }

        $whereArray=array('distcode'=>$distcode, 'projectcode'=>$projcode, 'sectorcode'=>$sectcode, 'awccode'=>$awccode, 'active'=>1, 'family_code'=>$family_code, 'member_id' => $family_id);

        $familywhere=array('distcode'=>$distcode, 'projectcode'=>$projcode, 'sectorcode'=>$sectcode, 'awccode'=>$awccode, 'code'=>$family_code, 'id'=>$family_id, 'target_code'=>$targetcode);

        if(($this->sql_helper->getCount($table, $whereArray)==0) || ($this->sql_helper->getCount('family_master', $familywhere)==0))
        {
            $this->sql_helper->update_data($table, array('active'=>0), $whereArray);
            $columnArray = array(
                'vhnd_date' => date("Y-m-d"),
                'vmonth' => date("m"),
                'vyear' => date("Y"),
                'family_code' => $family_code,
                'member_id' => $family_id,
                'target_code' => $targetcode,
                'lmp' => $lmp,
                'edd' => $edd,
                'registration_date' => $registration_date,
                'mcts_no' => $mctsno,
                'anc1due' => $anc1due,
                'anc2due' => $anc2due,
                'anc3due' => $anc3due,
                'anc4due' => $anc4due,
                'delivery_date' => $delv,
                'distcode' => $distcode,
                'projectcode' => $projcode,
                'sectorcode' => $sectcode,
                'totalboy_liv' => $bl,
                'totalboy_dead' => $bd,
                'totalgirl_liv' => $gl,
                'totalgirl_dead' => $gd,
                'delivery_type_id' => $delivtype,
                'awccode' => $awccode,
                'entry_by' => $loggeduser,
                'entry_date' => date("Y-m-d"),
                'ipaddress' => $_SERVER["REMOTE_ADDR"],
                'total_updation' => 0,
                'last_update_by' => $loggeduser,
                'last_update_date' => date('Y-m-d'),
                'active' => 1
            );
            $this->sql_helper->insert_data($table, $columnArray);
        }
        else
        {
            $cnt = $this->sql_helper->retText($table, 'total_updation', $whereArray)+1;

            $columnArray = array(
                'lmp' => $lmp,
                'edd' => $edd,
                'registration_date' => $registration_date,
                'mcts_no' => $mctsno,
                'anc1due' => $anc1due,
                'anc2due' => $anc2due,
                'anc3due' => $anc3due,
                'anc4due' => $anc4due,
                'delivery_date' => $delv,
                'totalboy_liv' => $bl,
                'totalboy_dead' => $bd,
                'totalgirl_liv' => $gl,
                'totalgirl_dead' => $gd,
                'delivery_type_id' => $delivtype,
                'last_update_by' => $loggeduser,
                'total_updation' => $cnt,
                'last_update_date' => date('Y-m-d'),
                'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

            $this->sql_helper->update_data($table, $columnArray, $whereArray);
        }

        /* code execute when lactating mothers has delivered baby **/
        if($targetcode == "L")
        {
            if($bl>0)
            {
                for($i=0; $i<$bl; $i++)
                {
                    $this->addbaby($family_id, 'M', $delv, $bl, $distcode, $projcode, $sectcode);
                }
            }
            if($gl>0)
            {
                for($i=0; $i<$gl; $i++)
                {
                    $this->addbaby($family_id, 'F', $delv, $gl, $distcode, $projcode, $sectcode);
                }
            }
        }

        $this->sql_helper->update_data("family_master", array('target_code'=>$targetcode), array('id'=>$family_id));
    }

    public function addbaby($member_id, $gender, $dob, $totbirth, $distcode, $projcode, $sectcode)
    {
        $table="family_master";
        $family = $this->get_memberdata($member_id, $distcode, $projcode, $sectcode)->row();

        $this->sql_helper->update_data($table, array('active'=>0), array('mother_id' => $member_id, 'auto_code' => $family->auto_code, 'dob' => $dob));

        if($this->sql_helper->getCount($table, array('mother_id' => $member_id, 'auto_code' => $family->auto_code, 'dob' => $dob))==0)
        {
            $columnArray = array(
                'code' => $family->code,
                'auto_code' => $family->auto_code,
                'is_residence' => $family->is_residence,
                'category_code' => $family->category_code,
                'gender' => strtoupper($gender),
                'dob' => $dob,
                'marital_status' => 'SINGLE',
                'is_head' => 'N',
                'disability_type' => 'NONE',
                'target_code' => 'C',
                'distcode' => $family->distcode,
                'projectcode' => $family->projectcode,
                'sectorcode' => $family->sectorcode,
                'awccode' => $family->awccode,
                'entry_date' => date("Y-m-d"),
                'ipaddress' => $_SERVER["REMOTE_ADDR"],
                'active' => 1,
                'mother_id' => $member_id,
                'weight' => 0,
                'height' => 0,
                'total_updation' => 0,
                'last_update_date' => date('Y-m-d'),
                'death_status' => 0,
                'isNew' => 1
            );
            $this->sql_helper->insert_data($table, $columnArray);
        }
        else
        {

            $whereArray = array(
            "mother_id='".$member_id."' and auto_code='".$family->auto_code."' and dob='".$dob."' and active='0' limit 1"=>NULL
            );
            $columnArray = array(
                'gender' => strtoupper($gender),
                'active' => 1,
                'last_update_by' => $this->session->userdata("user_id"),
                'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
                'last_update_date' => date('Y-m-d'),
                'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );
            $this->sql_helper->update_data($table, $columnArray, $whereArray);
        }
    }

    public function ret_existingtc($family_id, $distcode, $projectcode, $sectorcode, $awccode=NULL)
    {
        $table = "family_master";
        $retcolumn = "target_code";
        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["id"] = $family_id;

        return $this->sql_helper->retText($table, $retcolumn, $where);
    }

    public function ret_womenstatusdet($family_id, $family_code, $distcode, $projectcode, $sectorcode, $awccode)
    {
        $table = "womenstatus_master";
        $columns = "id,vhnd_date,vmonth,vyear,member_id,family_code,target_code,lmp,edd,registration_date,delivery_date,delivery_type_id,anc1due,anc2due,anc3due,anc4due,distcode,projectcode,sectorcode,awccode,active,mcts_no,totalboy_liv,totalboy_dead,totalgirl_liv,totalgirl_dead";

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["family_code"] = $family_code;
        $where["member_id"] = $family_id;
        $where["active"] = 1;

        $whereArray=array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, 'active'=>1, 'family_code'=>$family_code, 'member_id' => $family_id);

        $orderBy = array("id"=>"desc");
        return $this->sql_helper->getResult($table, $columns, $whereArray, $orderBy);
    }

    public function check_exitence($whereArray)
    {
        $table = "family_master f";
        return $this->sql_helper->getCount($table, $whereArray);
    }

    public function searched_list($where, $limit, $offset) {
        $table='family_master f';
        $column='f.id,f.code,f.auto_code,f.name,f.hindi_name,f.dob,f.adhar_no,f.target_code,f.awccode,f.isNew,f.active';
        $order=array('f.code'=>'asc', 'f.name'=>'asc');
        return $this->sql_helper->getDataList($table, $column, $where, $order, $limit, $offset);
    }

    public function count_family($where) {
        $table = 'family_master f';
        return $this->sql_helper->getCount($table, $where);
    }

    public function getauto_code($awccode, $distcode, $projectcode, $sectorcode)
    {
        $maxautocode = $this->sql_helper->getText('family_master', 'max(auto_code)', "distcode='$distcode' and projectcode='$projectcode' and sectorcode='$sectorcode' and awccode='$awccode'");

        if($maxautocode!=NULL && isset($maxautocode)){
        $famarr = explode('-', $maxautocode);
        $fam_id = $famarr[1];
        return $awccode . '-' . sprintf("%03d", ($fam_id+1));
        }
        return $awccode . '-' . sprintf("%03d", 1);
    }

    public function newfamily_add($autocode, $member_code, $is_resident, $caste_category, $member_name, $is_head, $mobile_no, $gender, $dob1, $marital_status, $adhar_no, $disability_type, $select_bentype, $weight, $height, $awccode, $distcode, $projectcode, $sectorcode)
    {
        $table="family_master";
        $columnArray = array(
            'code' => $member_code,
            'auto_code' => $autocode,
            'is_residence' => $is_resident,
            'category_code' => $caste_category,
            'name' => strtoupper($member_name),
            'gender' => strtoupper($gender),
            'dob' => $dob1,
            'marital_status' => strtoupper($marital_status),
            'is_head' => strtoupper($is_head),
            'adhar_no' => strtoupper($adhar_no),
            'disability_type' => strtoupper($disability_type),
            'mobile_no' => $mobile_no,
            'target_code' => strtoupper($select_bentype),
            'weight' => $weight,
            'height' => $height,
            'death_status' => 0,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'entry_date' => date("Y-m-d"),
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'total_updation' => 0,
            'last_update_date' => date('Y-m-d'),
            'active' => 1,
            'isNew' => 1
        );

        $this->sql_helper->insert_data($table, $columnArray);
    }

    /*=======women target update master ======*/
    public function get_deliverytype($where) {
        $table='deliverytype_master';
        $column='id,delivery_type';
        return $this->sql_helper->getResult($table, $column, $where);
    }

    /**========get member with no adhar list **/
    public function get_benwithnoadhar($select_target, $distcode,$projcode,$sectcode,$awccode){

        $table = "family_master";
        $columns = "id, code, auto_code, awccode, name, dob, marital_status, gender, adhar_no, target_code";
        $where = array("distcode"=>$distcode,"projectcode"=>$projcode,"sectorcode"=>$sectcode, "awccode"=>$awccode, "target_code"=>$select_target, "active"=>1, "(length(adhar_no) != 12 or adhar_no is null or adhar_no='')" =>NULL);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function checkonvaccine($fcode, $awccode, $distcode, $projcode, $sectcode)
    {

        $table = "vhndvaccine_details";
        $columns = "id";
        $where = array("distcode"=>$distcode,"projectcode"=>$projcode,"sectorcode"=>$sectcode,"awccode"=>$awccode,"family_code"=>$fcode);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function checkonanc($fcode, $awccode, $distcode, $projcode, $sectcode)
    {

        $table = "womenanccheckup_details";
        $columns = "id";
        $where = array("distcode"=>$distcode,"projectcode"=>$projcode,"sectorcode"=>$sectcode,"awccode"=>$awccode,"family_code"=>$fcode);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_allmember($auto_code, $distcode, $projcode, $sectcode, $awccode)
    {
        $table = "family_master";
        $columns = "id,code,auto_code,is_residence,category_code,survey_date,name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,weight,target_code,distcode,projectcode,sectorcode,awccode,entry_by,entry_date,ipaddress,active,height,muac,father_id,fh_name,mother_id,mother_name,death_status,death_date,death_remarks,child_sequence,last_update_date,last_update_by,last_update_role,total_updation,relation,isNew";
        $where = array("distcode"=>$distcode,"projectcode"=>$projcode,"sectorcode"=>$sectcode,"awccode"=>$awccode,"auto_code"=>$auto_code);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function insert_deletemember($columnArray)
    {
        $table="deletedmember_master";
        return $this->sql_helper->insert_data($table, $columnArray);
    }

    public function delete_fmember($fid, $auto_code, $distcode, $projcode, $sectcode, $awccode)
    {
        $table="family_master";
        $where = array("distcode"=>$distcode,"projectcode"=>$projcode,"sectorcode"=>$sectcode,"awccode"=>$awccode,"auto_code"=>$auto_code, "id"=>$fid);
        return $this->sql_helper->delete_data($table, $where);
    }


    public function insert_inverification($columnArray)
    {
        $table="verifiedmasterdata_master";
        return $this->sql_helper->insert_data($table, $columnArray);
    }

    public function ret_varifieddata($family_id, $distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'verifiedmasterdata_master';
        $columns = 'verf_status';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $where["family_id"] = $family_id;

        $order = array('id' => 'desc');
        return $this->sql_helper->getResult($table, $columns, $where, $order);
    }

    public function insert_expectinmodel($columnArray)
    {
        $table="verifiedexpecteddata_master";
        return $this->sql_helper->insert_data($table, $columnArray);
    }

    public function update_expectinmodel($columnArray, $whereArray)
    {
        $table="verifiedexpecteddata_master";
        return $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }

    public function exptdata($distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'verifiedexpecteddata_master';
        $columns = 'p_expected_member, l_expected_member, c_expected_member, a_expected_member';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        $order = array('id' => 'desc');
        return $this->sql_helper->getResult($table, $columns, $where, $order);
    }

}
