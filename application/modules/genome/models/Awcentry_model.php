<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Awcentry_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*Code for Anganbari*/
    public function get_anganbari($sector, $awccode=NULL) {
        $table = 'awc_master';
        $columns = 'code,name';
        $where = array('sectorcode'=>$sector, 'active'=>1);
        if(isset($awccode))
            $where = array('sectorcode'=>$sector, 'code'=>$awccode, 'active'=>1);
        $orderBy = array('name'=>'asc');
        return $this->sql_helper->getArray($table, $columns, $where, $orderBy);
    }

     public function get_anganbariresult($sector, $awccode=NULL) {
        $table = 'awc_master';
        $columns = 'code,name,name_hi,u_code,sv_name,awc_type_id,society_type_id,construction_type_id,buildingarea_type_id,drinkingwater_avail,toilet_avail,waterontoilet_avail,langitude,latitude,distcode,projectcode,sectorcode';
        $where = array('sectorcode'=>$sector, 'active'=>1);
        if(isset($awccode))
            $where = array('sectorcode'=>$sector, 'code'=>$awccode, 'active'=>1);
        $orderBy = array('name'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderBy);
    }

    public function get_lastupdatedrdate($awccode)
    {
        $table = "awcservice_master";
        $retcolumn = "date_format(interval 1 day + record_date, '%d-%m-%Y')";
        $where = "active=1 and awccode='".$awccode."' order by id desc";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }
    
    public function ret_awcemp($emptype_id, $awccode)
    {
        $table = "awcemp_details";
        $retcolumn = "avaibility";
        $where = array("emp_type_id"=>$emptype_id, "awccode"=>$awccode);
        return $this->sql_helper->retText($table, $retcolumn, $where);
    }

    public function ret_availawcemp($awccode) {
        $table = 'employee_type_master ';
        $columns = 'id, emp_type';
        $where = array('active'=>1, 'id in (select emp_type_id from ci_awcemp_details where avaibility=1 and awccode=\''.$awccode.'\')'=>NULL);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*Code for AWC Type*/
    public function get_awctype() {
        $table = 'awctype_master';
        $columns = 'id, awc_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*Code for Socity Type*/
    public function get_societytype() {
        $table = 'society_master';
        $columns = 'id, society_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*Code for construction Type*/
    public function get_constructiontype() {
        $table = 'buildingconstruction_master';
        $columns = 'id, construction_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*Code for Building Type*/
    public function get_buildingtype() {
        $table = 'buildingarea_master';
        $columns = 'id, area_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*Code for beneficiary Type*/
    public function get_beneficiarytype() {
        $table = 'beneficiarytype_master';
        $columns = 'id, name, service_age, service_period, expiry_age, expiry_period, target_code';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_beneficiarytypeid()
    {
        $table = "beneficiarytype_master";
        $retcolumn = "id";
        $where = "active=1 and (service_age=3 and service_period='YEAR') and  (expiry_age=6 and expiry_period='YEAR')";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }
    
    public function get_beneficiarytypename()
    {
        $table = "beneficiarytype_master";
        $retcolumn = "name";
        $where = "active=1 and (service_age=3 and service_period='YEAR') and  (expiry_age=6 and expiry_period='YEAR')";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }
    
    /*Code for service Type*/
    public function get_servicetype() {
        $table = 'servicetype_master';
        $columns = 'id, service_type';
        $where = array('active'=>1);
        $orderby = array('id'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }
    
    /*Code for service*/
    public function get_service() {
        $table = 'service_master s';
        $columns = 's.id, s.service_name, s.service_type_id, t.service_type, s.acronym';
        $join = array('servicetype_master t'=>"s.service_type_id=t.id");
        $where = array('s.active'=>1, "s.id!=3"=>NULL);
        $orderby = array('s.service_type_id'=>'asc');
        return $this->sql_helper->getJoinResult($table, $columns, $join, $where, $orderby);
    }
    
    /*Code for employee Type*/
    public function get_employeetype() {
        $table = 'employee_type_master ';
        $columns = 'id, emp_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    /*get family data */
    public function get_family($awccode) {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,is_residence,category_code,name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,weight,target_code,height,muac,father_id,mother_id,death_status,death_date,death_remarks,child_sequence,relation,distcode,projectcode,sectorcode,awccode,active';
        $where = array('active'=>1, "awccode"=>$awccode);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function insert_anganbari($columnArray)
    {
        $table = "awc_master";
        $this->sql_helper->insert_data($table, $columnArray);
    }
    
    public function update_anganbari($columnArray, $whereArray)
    {
        $table = "awc_master";
        $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }
    
    public function awcdetails_entry($emptype_id, $avaibility, $awccode)
    {
        $table="awcemp_details";
        
        $columnArray = array(
        'emp_type_id' => $emptype_id,
        'avaibility' => $avaibility,
        'entry_date' => date("Y-m-d"),
        'awccode' => $awccode
        );

        $whereArray = array(
        'emp_type_id' => $emptype_id,
        'awccode' => $awccode
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
            $this->sql_helper->insert_data($table, $columnArray);
        else
            $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }
    
    public function gen_awcservicecode($awccode, $bentypeid)
    {
        $table = "awcservice_master";
        
        $whereArray = array(
        'beneficiarytype_id' => $bentypeid,
        'awccode' => $awccode
        );
        
        $total_data = $this->sql_helper->getCount($table, $whereArray) + 1;
        $code_id = str_pad($total_data, 2, "0", STR_PAD_LEFT);
        $pad_bentypeid = str_pad($bentypeid, 2, "0", STR_PAD_LEFT);
        
        $unique_code = $awccode . "" . $pad_bentypeid . "" . $code_id;
        
        return $unique_code;
    }
    
    public function awcservice_entry($awccode, $distcode, $projectcode, $sectorcode, $bentypeid, $record_date)
    {
        $table = "awcservice_master";
        
        $whereArray = array(
        'awccode' => $awccode,
        'beneficiarytype_id' => $bentypeid,
        'record_date' => $record_date
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $columnArray = array(
            'code' => $this->gen_awcservicecode($awccode, $bentypeid),
            'record_date' => $record_date,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'beneficiarytype_id' => $bentypeid,
            'entry_by' => $this->session->userdata("user_id"),
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'active' => 1
            );
            $this->sql_helper->insert_data($table, $columnArray);
        }
    }
    
    public function ret_awcserviceid($distcode, $projectcode, $sectorcode, $awccode, $bentypeid, $record_date)
    {
        $table = "awcservice_master";
        $retcolumn = "id";
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, "awccode"=>$awccode, "beneficiarytype_id"=>$bentypeid, "record_date"=>$record_date);
        return $this->sql_helper->retText($table, $retcolumn, $where);
    }

    /*Code for family record*/
    public function get_3to6child($awccode, $distcode, $projectcode, $sectorcode) {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,is_residence,category_code,name,gender,dob,marital_status,is_head,adhar_no,disability_type,mobile_no,weight,target_code,height,muac,father_id,mother_id,death_status,death_date,death_remarks,child_sequence,relation,distcode,projectcode,sectorcode,awccode,active';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, 'active'=>1, 'dob <= (CURRENT_DATE() - INTERVAL 3 YEAR)'=>NULL, 'dob > (CURRENT_DATE() - INTERVAL 6 YEAR)'=>NULL);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function awcservicedet_entry($awcservice_id, $service_id,  $family_code, $family_id, $attend_flag)
    {
        $table = "awcservice_details";
        
        $whereArray = array(
        'awcservice_id' => $awcservice_id,
        'service_id' => $service_id,
        'member_id' => $family_id,
        'family_code' => $family_code
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {

            $serColumn = array(
            'awcservice_id' => $awcservice_id,
            'service_id' => $service_id,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'attendnce' => $attend_flag
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {
            $serColumn = array(
            'awcservice_id' => $awcservice_id,
            'service_id' => $service_id,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'attendnce' => $attend_flag,
            'last_update_by' => $this->session->userdata("user_id"),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }
    }

    public function awcserviceempatt_entry($awcservice_id, $emptype_id, $attend_flag)
    {
        $table = "awcempservice_attandance";
        
        $whereArray = array(
        'awcservice_id' => $awcservice_id,
        'emp_type_id' => $emptype_id
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $serColumn = array(
            'awcservice_id' => $awcservice_id,
            'emp_type_id' => $emptype_id,
            'attendnce' => $attend_flag
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {
            
            $serColumn = array(
            'awcservice_id' => $awcservice_id,
            'emp_type_id' => $emptype_id,
            'attendnce' => $attend_flag,
            'last_update_by' => $this->session->userdata("user_id"),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }
    }
    
    /*Code for employee Type*/
    public function ret_totservices($sertype_id) {
        $table = 'service_master ';
        $where = array('active'=>1, "id!=3"=>NULL, "service_type_id"=>$sertype_id);
        return $this->sql_helper->getCount($table, $where);
    }
    
    /*Code for awc service details*/
    public function get_awcsericedet($awcservice_id) {
        $table = 'awcservice_details ';
        $columns = 'id, awcservice_id,service_id,member_id,family_code,attendnce';
        $where = array("awcservice_id"=>$awcservice_id);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function get_awcserviceemp($awcservice_id){
        $table = "awcempservice_attandance";
        $columns = 'id,awcservice_id,emp_type_id,attendnce';
        $where = array("awcservice_id"=>$awcservice_id);
        return $this->sql_helper->getResult($table, $columns, $where);
    }


    /* get data by bulk attendance */
    public function get_bulkattendance($Ymd, $member_id, $awccode, $service_id)
    {
        $table = 'awcservice_details s';
        $columns = 's.id, s.awcservice_id, m.beneficiarytype_id, m.record_date, m.distcode, m.projectcode, m.sectorcode,m.awccode, s.service_id, s.member_id, s.family_code, s.attendnce as attendnce';
        $join = array('awcservice_master m'=>"s.awcservice_id=m.id");
        $where = array("m.record_date"=>$Ymd, "s.member_id"=>$member_id, "m.awccode"=>$awccode, "s.service_id"=>$service_id);
        return $this->sql_helper->getJoinResult($table, $columns, $join, $where);
    }
    

     /**
     *
     * Code for holiday master
     *
     **/
    public function get_holiday() {
        $table = 'holiday_master ';
        $columns = 'id, holiday_name, description';
        $where = array('active'=>1);
        return $this->sql_helper->getArray($table, $columns, $where);
    }
    
    public function count_holiday() {
        $table = 'holiday_master ';
        return $this->sql_helper->getCount($table, array("active"=>1));
    }
    
    public function count_holidaybyname($hname) {
        $table = 'holiday_master ';
        $columns = '*';
        $where = array('active'=>1, 'holiday_name'=>$hname);
        return $this->sql_helper->getCount($table, $where);
    }
    
    public function insert_holidaymaster($columnArray)
    {
        $table = "holiday_master";
        $this->sql_helper->insert_data($table, $columnArray);
    }
    
    public function update_holidaymaster($columnArray, $whereArray)
    {
        $table = "holiday_master";
        $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }
}