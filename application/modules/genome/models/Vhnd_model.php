<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vhnd_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

      /*Code for Anganbari*/
    public function get_anganbari($args, $awccode=NULL) {
        $table = 'awc_master';
        $columns = 'code, name';
        $where = array('sectorcode'=>$args, 'active'=>1);
        if(isset($awccode))
            $where = array('code'=>$awccode, 'active'=>1);
        $orderBy = array('name'=>'asc');
        return $this->sql_helper->getArray($table, $columns, $where, $orderBy);
    }
    
    public function get_disabilitytype() {
        $table = 'disability_master';
        $columns = 'disability_type';
        $where = array('active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function check_exitence($whereArray)
    {
        $table = "family_master f";
        return $this->sql_helper->getCount($table, $whereArray);
    }

    /*Code for beneficiary Type*/
    public function get_beneficiarytype() {
        $table = 'beneficiarytype_master';
        $columns = 'id,name,service_age,service_period,expiry_age,expiry_period,target_code,active';
        $where = array('active'=>1, 'target_code'=>'C');
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_targetcode($bid) {
        $table = 'beneficiarytype_master';
        $columns = 'id,name,service_age,service_period,expiry_age,expiry_period,target_code';
        $where = array('active'=>1, "id"=>$bid);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function family_alter($family_id, $member_name, $hindi_name, $gender, $dob, $adhar_no, $disability_type, $father_id, $mother_id, $distcode, $projcode, $sectcode, $awccode)
    {
        $table="family_master";
        
        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projcode,
        'sectorcode' => $sectcode,
        'awccode' => $awccode,
        'id' => $family_id
        );

        if($this->sql_helper->getCount($table, $whereArray)>0)
        {

            $father_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $father_id));
            if(!isset($father_name))
                $father_name = "";

            $mother_name = $this->sql_helper->retText($table, 'name', array('distcode' => $distcode,'projectcode' => $projcode,'sectorcode' => $sectcode,'awccode' => $awccode,'id' => $mother_id));
            if(!isset($mother_name))
                $mother_name = "";

             $columnArray = array(
                'name' => strtoupper($member_name),
                'hindi_name' => strtoupper($hindi_name),
                'gender' => strtoupper($gender),
                'dob' => $dob,
                'adhar_no' => $adhar_no,
                'disability_type' => strtoupper($disability_type),
                'target_code' => "C",
                'father_id' => $father_id,
                'fh_name' => strtoupper($father_name), 
                'relation' => "FATHER",
                'mother_id' => $mother_id,
                'mother_name' => strtoupper($mother_name),
                'last_update_by' => $this->session->userdata("user_id"),
                'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
                'last_update_date' => date('Y-m-d'),
                'ipaddress' => $_SERVER["REMOTE_ADDR"],
                'active' => 1
                );

            if($adhar_no=="*")
            {
                $columnArray = array(
                'name' => strtoupper($member_name),
                'hindi_name' => strtoupper($hindi_name),
                'gender' => strtoupper($gender),
                'dob' => $dob,
                'disability_type' => strtoupper($disability_type),
                'target_code' => "C",
                'father_id' => $father_id,
                'fh_name' => strtoupper($father_name), 
                'relation' => "FATHER",
                'mother_id' => $mother_id,
                'mother_name' => strtoupper($mother_name),
                'last_update_by' => $this->session->userdata("user_id"),
                'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
                'last_update_date' => date('Y-m-d'),
                'ipaddress' => $_SERVER["REMOTE_ADDR"],
                'active' => 1
                );
            }

            $this->sql_helper->update_data($table, $columnArray, $whereArray);
        }
    }

    /*get family data */
    public function get_family($awccode, $service_age, $service_period, $expiry_age, $expiry_period, $target_code, $distcode, $projectcode, $sectorcode) 
    {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,name,gender,dob,marital_status,is_head,adhar_no,mobile_no,weight,target_code,height,muac,father_id,mother_id,distcode,projectcode,sectorcode,awccode,active';

        $where = array();

        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;
        
        $where["target_code"] = $target_code;

        if($target_code == "C")
        {
            $where["dob <= (CURRENT_DATE() - INTERVAL ".$service_age." ".$service_period.")"] = NULL;
            $where["dob > (CURRENT_DATE() - INTERVAL ".$expiry_age." ".$expiry_period.")"] = NULL;
        }

        $where["active"] = 1;

        $orderby = array('name' => 'asc');
        
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }
    
    public function get_fmember($family_code, $member_name, $distcode, $projectcode, $sectorcode) {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,name,gender,dob,marital_status,is_head,adhar_no,mobile_no,weight,target_code,height,muac,father_id,mother_id,distcode,projectcode,sectorcode,awccode,active';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'active'=>1, "code"=>$family_code, "name"=>$member_name);

        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_othermember($id, $autocode, $gender, $distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'family_master';
        $columns = 'id,code,name';
        
        $where = array();
        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        if($autocode != NULL)
            $where["auto_code"] = $autocode;

        if($id != NULL)
            $where["id!="] = $id;

        if($gender != NULL)
            $where["gender"] = $gender;

        return $this->sql_helper->getResult($table, $columns, $where);
    }   

    public function get_memberdata($fid, $distcode, $projectcode, $sectorcode, $awccode=NULL) {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,name,hindi_name,gender,dob,marital_status,adhar_no,mobile_no, ,target_code,father_id,mother_id,disability_type,distcode,projectcode,sectorcode,awccode,active';
        
        $where = array();
        if($distcode>0)
             $where["distcode"] = $distcode;
        if($projectcode>0)
            $where["projectcode"] = $projectcode;
        if($sectorcode>0)
            $where["sectorcode"] = $sectorcode;

        if(isset($awccode))
            if($awccode>0)
                $where["awccode"] = $awccode;

        if($fid != NULL)
            $where["id"] = $fid;

        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function ret_familyhead($autocode, $distcode, $projectcode, $sectorcode, $awccode) {
        $table = 'family_master ';
        $columns = 'name';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode,  "auto_code"=>$autocode, "is_head"=>"Y");

        return $this->sql_helper->retText($table, $columns, $where);
    }
    
    public function get_vaccine($vid) {
        $table = 'vaccine_master';
        $columns = 'id,vaccine_name,protection,does_no,vacronym';
        $where = array('active'=>1, "id"=>$vid);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_doses($whereArray=NULL) {
        $table = 'immunization_master';
        $columns = 'id,vid,doses_no,schedule_min,schedule_max,schedule_format';
        return $this->sql_helper->getResult($table, $columns, $whereArray);
    }
    
    public function get_vaccinedet($fid) {
        $table = 'vhndvaccine_details ';
        $columns = 'id,vhnd_date,vmonth,vyear,beneficiarytype_id,member_id,family_code,vid,doses_no,due_date,delivery_date,delivery_update_by,delivery_update_date,attendance,distcode,projectcode,sectorcode,awccode,active';
        $where = array('active'=>1, "member_id"=>$fid, "due_date<=now()"=>NULL);
        $orderby = array('due_date'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }
    
    public function get_vhndwtdet($awccode, $bentypeid, $record_month, $record_year, $familyid)
    {
        $table = 'vhndweight_details';
        $columns = '*';
        $where = array("awccode"=>$awccode, "beneficiarytype_id"=>$bentypeid, "vmonth"=>$record_month, "vyear"=>$record_year, "member_id"=>$familyid,'active'=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function vhndweightdeatils_entry($bentypeid, $record_date, $record_month, $record_year, $family_id, $family_code, $weight, $height, $muac, $thr_flag, $attend_flag, $awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = "vhndweight_details";
        
        $whereArray = array(
        'vmonth' => $record_month,
        'vyear' => $record_year,
        'member_id' => $family_id
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $serColumn = array(
            'vhnd_date' => $record_date,
            'vmonth' => $record_month,
            'vyear' => $record_year,
            'beneficiarytype_id' => $bentypeid,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'weight' => $weight,
            'height' => $height,
            'muacincm' => $muac,
            'thr' => $thr_flag,
            'attendance' => $attend_flag,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'entry_by' => $this->session->userdata("user_id"),
            'total_updation' => 0,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {
            $serColumn = array(
            'weight' => $weight,
            'height' => $height,
            'muacincm' => $muac,
            'thr' => $thr_flag,
            'attendance' => $attend_flag,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );
           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }

        $this->sql_helper->update_data("family_master", array('height'=>$height, 'muac'=>$muac, 'weight'=>$weight), array('id'=>$family_id));
    }
    
    public function vhndvaccinedeatils_entry($bentypeid, $record_date, $record_month, $record_year, $family_code, $family_id, $vid, $doses_no, $duedt, $awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = "vhndvaccine_details";
        
        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projectcode,
        'sectorcode' => $sectorcode,
        'awccode' => $awccode,
        'member_id' => $family_id,
        'vid' => $vid,
        'doses_no' => $doses_no
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $serColumn = array(
            'vhnd_date' => $record_date,
            'vmonth' => $record_month,
            'vyear' => $record_year,
            'beneficiarytype_id' => $bentypeid,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'vid' => $vid,
            'doses_no' => $doses_no,
            'due_date' => $duedt,
            'attendance' => 0,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'entry_by' => $this->session->userdata("user_id"),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );
            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {
            $serColumn = array(
            'vid' => $vid,
            'doses_no' => $doses_no,
            'due_date' => $duedt,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );
           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }
    }
    
    public function vhndvaccinedel_entry($vhnd_id, $deldt, $family_id, $awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = "vhndvaccine_details";
        
        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projectcode,
        'sectorcode' => $sectorcode,
        'awccode' => $awccode,
        'member_id' => $family_id,
        'id' => $vhnd_id
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        { 

            $serColumn = array(
            'delivery_date' => $deldt,
            'delivery_update_by' => $this->session->userdata("user_id"),
            'delivery_update_date' => date("Y-m-d"),
            'attendance' => 1,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => 0,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {

            $serColumn = array(
            'delivery_date' => $deldt,
            'delivery_update_by' => $this->session->userdata("user_id"),
            'delivery_update_date' => date("Y-m-d"),
            'attendance' => 1,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }
    }
    
    public function get_vhndvaccinedet($familyid)
    {
        $table = 'vhndvaccine_details';
        $columns = 'id,vhnd_date,vmonth,vyear,beneficiarytype_id,member_id,family_code,vid,doses_no,due_date,delivery_date,delivery_update_by,delivery_update_date,attendance,distcode,projectcode,sectorcode,awccode,active';
        $where = array('member_id'=>$familyid);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_lactatinglist($awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = 'family_master ';
        $columns = 'id,code,auto_code,name,gender,dob,target_code,distcode,projectcode,sectorcode,awccode,active';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'active'=>1, "awccode"=>$awccode, 'gender'=>'F');
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_womenstatus() {
        $table = 'womenstatus_master';
        $columns = 'id,vhnd_date,vmonth,vyear,member_id,family_code,target_code,lmp,edd,registration_date,delivery_date,anc1due,anc2due,anc3due,anc4due,distcode,projectcode,sectorcode,awccode,active,mcts_no,totalboy_liv,totalboy_dead';
        $where = array("target_code"=>'P', "active"=>1);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function get_womenstatusdata($id) {
        $table = 'womenstatus_master';
        $columns = 'id,vhnd_date,vmonth,vyear,member_id,family_code,target_code,lmp,edd,registration_date,delivery_date,anc1due,anc2due,anc3due,anc4due,distcode,projectcode,sectorcode,awccode,active,mcts_no,totalboy_liv,totalboy_dead';
        $where = array("id"=>$id);
        return $this->sql_helper->getResult($table, $columns, $where)->row();
    }
    
    public function get_vhndlactdet($awccode, $bentypeid, $record_month, $record_year) {
        $table = 'vhndlactatt_details ';
        $columns = 'id,vhnd_date,vmonth,vyear,beneficiarytype_id,member_id,family_code,thr,attendance,distcode,projectcode,sectorcode,awccode,active';
        $where = array("awccode"=>$awccode, "beneficiarytype_id"=>$bentypeid, "vmonth"=>$record_month, "vyear"=>$record_year);
        return $this->sql_helper->getResult($table, $columns, $where);
    }
    
    public function vhndlactating_entry($bentypeid, $record_date, $record_month, $record_year, $family_code, $family_id, $thr_flag, $attend_flag, $awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = "vhndlactatt_details";
        
        $whereArray = array(
        'vmonth' => $record_month,
        'vyear' => $record_year,
        'member_id' => $family_id
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $serColumn = array(
            'vhnd_date' => $record_date,
            'vmonth' => $record_month,
            'vyear' => $record_year,
            'beneficiarytype_id' => $bentypeid,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'thr' => $thr_flag,
            'attendance' => $attend_flag,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'entry_by' => $this->session->userdata("user_id"),
            'total_updation' => 0,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {
            $serColumn = array(
            'thr' => $thr_flag,
            'attendance' => $attend_flag,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }
    }
    
    /* women pregnant data */

    public function get_pregnantwomenlist($awccode, $distcode, $projectcode, $sectorcode)
    {
        $table = 'womenstatus_master s ';
        $columns = "s.id, s.vhnd_date, s.vmonth, s.vyear, s.member_id, f.code, f.auto_code, f.name, f.adhar_no, f.mobile_no, "
        ."f.dob, s.target_code, s.lmp, s.edd, s.registration_date, s.delivery_date, s.anc1due, s.anc2due, s.anc3due, s.anc4due, "
        ."s.distcode, s.projectcode, s.sectorcode, s.awccode ";
        $join = array("ci_family_master f"=>"s.member_id=f.id and s.active=1");
        $where = array('s.distcode'=>$distcode, 's.projectcode'=>$projectcode, 's.sectorcode'=>$sectorcode, "s.awccode"=>$awccode, "s.active"=>1, "f.target_code"=>'P');
        return $this->sql_helper->getJoinResult($table, $columns, $join, $where);
    }
    
    public function get_anccheckupdata($awccode, $record_month, $record_year) {
        $table = 'womenanccheckup_details ';
        $columns = 'id,womenstatus_id,vhnd_date,vmonth,vyear,member_id,family_code,thr,anc1delivery,anc2delivery,anc3delivery,anc4delivery,ifa_count,distcode,projectcode,sectorcode,awccode,active';
        //$where = array("awccode"=>$awccode, "vmonth"=>$record_month, 'vyear'=>$record_year);
        $where = array("awccode"=>$awccode);
        $orderby = array("id"=>"desc");
        return $this->sql_helper->getResult($table, $columns, $where, $orderby);
    }
    
    public function vhndpreg_entry($vhnd_id, $family_id, $family_code, $record_date, $record_month, $record_year, $thr_flag, $anc1, $anc2, $anc3, $anc4, $delivery_date, $ifacount, $awccode, $distcode, $projectcode, $sectorcode, $loggeduser)
    {
        $table = "womenanccheckup_details";
        
        $whereArray = array(
        'distcode' => $distcode,
        'projectcode' => $projectcode,
        'sectorcode' => $sectorcode,
        'awccode' => $awccode,
        'womenstatus_id' => $vhnd_id,
        'member_id' => $family_id
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
        {

            $serColumn = array(
            'womenstatus_id' => $vhnd_id,
            'vhnd_date' => $record_date,
            'vmonth' => $record_month,
            'vyear' => $record_year,
            'member_id' => $family_id,
            'family_code' => $family_code,
            'thr' => $thr_flag,
            'anc1delivery'=>$anc1,
            'anc2delivery'=>$anc2,
            'anc3delivery'=>$anc3,
            'anc4delivery'=>$anc4,
            'ifa_count'=>$ifacount,
            'distcode' => $distcode,
            'projectcode' => $projectcode,
            'sectorcode' => $sectorcode,
            'awccode' => $awccode,
            'entry_by' => $this->session->userdata("user_id"),
            'total_updation' => 0,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

            $this->sql_helper->insert_data($table, $serColumn);
        }
        else
        {

            $serColumn = array(
            'thr' => $thr_flag,
            'anc1delivery'=>$anc1,
            'anc2delivery'=>$anc2,
            'anc3delivery'=>$anc3,
            'anc4delivery'=>$anc4,
            'ifa_count'=>$ifacount,
            'last_update_by' => $this->session->userdata("user_id"),
            'total_updation' => $this->sql_helper->retText($table, 'total_updation', $whereArray)+1,
            'last_update_date' => date('Y-m-d'),
            'ipaddress' => $_SERVER["REMOTE_ADDR"]
            );

           $this->sql_helper->update_data($table, $serColumn, $whereArray); 
        }

        $wsWhere=array(
        'distcode' => $distcode,
        'projectcode' => $projectcode,
        'sectorcode' => $sectorcode,
        'awccode' => $awccode,
        'member_id' => $family_id,
        'id'=>$vhnd_id
        );
        
        if($delivery_date!=NULL)
        $this->sql_helper->update_data("womenstatus_master", array("delivery_date"=>$delivery_date), $wsWhere); 
    }
    
    
    public function get_beneficiarytypeid($targetcode)
    {
        $table = "beneficiarytype_master";
        $retcolumn = "id";
        $where = "active=1 and target_code='".$targetcode."'";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }
    
    public function get_beneficiarytypename($targetcode)
    {
        $table = "beneficiarytype_master";
        $retcolumn = "name";
        $where = "active=1 and target_code='".$targetcode."'";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }


    /* Code for show Graph */

    //BMI
    public function get_bmidata($gender)
    {
        $table = "bmi_master";
         $columns = "age, girl_weight_normal_max as nmax, girl_weight_normal_min as nmin, girl_weight_medium_min as mmin";
        if($gender=='M')
            $columns = "age, boy_weight_normal_max as nmax, boy_weight_normal_min as nmin, boy_weight_medium_min as mmin";
        return $this->sql_helper->getArray($table, $columns);
    }

    public function get_childbmiwtdata($mid) {
        $table = 'family_master f';
        $columns = 'w.muacincm, w.weight, w.vhnd_date, f.dob, TIMESTAMPDIFF(MONTH, f.dob, w.vhnd_date) as month_age';
        $join = array('vhndweight_details w'=>"f.id=w.member_id and f.code=w.family_code");
        $where = array("f.id"=>$mid);
        $orderby = array('w.vhnd_date'=>'asc');
        return $this->sql_helper->getJoinArray($table, $columns, $join, $where, $orderby);
    }


    //MUAC
    public function get_muacdata()
    {
        $table = "muac_master";
        $columns = "id,size_fromcm,size_tocm,zone";        
        return $this->sql_helper->getArray($table, $columns);
    }

    //Inpsection Role
    public function get_inspectionrole($role)
    {
        $table = 'inspectionrole_details';
        $columns = 'id,inspection_role';

        $where = array('active'=>1);
        
        if(isset($role) && $role!=NULL)
            $where = array('active'=>1, 'id!='=>$role);
        
        return $this->sql_helper->getResult($table, $columns, $where);
        
    }

    public function get_inspectionbyname($id)
    {
        $table = 'inspectionrole_details';
        $columns = 'id,inspection_role';
        $where = array('active'=>1, 'id'=>$id);
        
        return $this->sql_helper->getResult($table, $columns, $where);
        
    }

    public function get_jointinspection($id, $roleid)
    {
        $table = 'jointinspection_details';
        $columns = 'id,inspect_id,inspuser_id,insprole_id,avaibility';
        $where = array('inspect_id'=>$id, 'insprole_id'=>$roleid);
        
        return $this->sql_helper->getResult($table, $columns, $where);
        
    }

    public function list_inspection()
    {
        $table = 'inspection_details ';
        $columns = '*';
        $where = array('active'=>1, 'entry_by' => $this->session->userdata("user_id"));
        $order = array('id'=>'desc');
        return $this->sql_helper->getResult($table, $columns, $where, $order);
    }

    public function get_toilettype()
    {
        $table = 'toilettype_master';
        $columns = 'id,toilet_type';
        $where = array('active'=>1);
        
        return $this->sql_helper->getResult($table, $columns, $where);
    }

    public function update_inspectiondata($insertArray, $updateArray, $whereArray, $jointrole)
    {
        $table = 'inspection_details';
        $id = 0;
        if($this->sql_helper->getCount($table, $whereArray)==0)
        {
            $id = $this->sql_helper->insert_data($table, $insertArray);
        }
        else
        {
            $this->sql_helper->update_data($table, $updateArray, $whereArray);
            $id = $this->sql_helper->retText($table, 'id', $whereArray);
        }

        if($id > 0)
        {
            if(isset($jointrole) && $jointrole!=NULL)
            {
                foreach ($jointrole as $key => $value) {
                    $insprole_id = $value;
                    $avaibility = 0;

                    if(isset($insprole_id) && $insprole_id!=NULL)
                        $avaibility = 1;

                    $colmnArray = array('inspect_id' => $id, 'insprole_id' => $insprole_id, 'avaibility' => $avaibility);

                    if($this->sql_helper->getCount('jointinspection_details', array('inspect_id' => $id, 'insprole_id' => $insprole_id))==0)
                        $this->sql_helper->insert_data('jointinspection_details', $colmnArray);
                    else
                        $this->sql_helper->update_data('jointinspection_details', $colmnArray, array('inspect_id' => $id, 'insprole_id' => $insprole_id)); 
                }
            }
        }
    }

    public function get_inspectiondet($id)
    {
        $table = 'inspection_details';
        $columns = '*';
        $where = array('id'=>$id);
        
        return $this->sql_helper->getResult($table, $columns, $where);
    }
}