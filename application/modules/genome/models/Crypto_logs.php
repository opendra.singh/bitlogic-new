<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crypto_logs extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_deposit_log( $crypto_username, $total, $currency, $crypto_selected_coin, $crypto_total, $invoice_id, $nonce, $crypto_coin_address, $address_in, $prefix_url ) {

        $data = array(
            'user_id' => $crypto_username,
            'amount' => $total,
            'currency' => $currency,
            'crypto_coin' => $crypto_selected_coin,
            'crypto_amount' => $crypto_total,
            'crypto_amount_remaining' => '',
            'status' => 'pending',
            'order_id' => $invoice_id,
            'order_procced_time' => date('Y-m-d H:i:s'),
            'payment_received_time' => '',
            'nonce' => $nonce,
            'coin_address' => $crypto_coin_address,
            'coin_address_in' => $address_in,
            'prefix_url' => $prefix_url,
        );

        $this->db->where('order_id', $invoice_id);

        $query = $this->db->get(DB_PREFIX .'crypto_deposit');

        $results = $query->result_array();
        
        if( isset( $results ) && empty( $results ) && is_array( $results ) ){

            $this->db->insert(DB_PREFIX .'crypto_deposit', $data);
        }

    }

    public function insert_withdraw_log( $crypto_username, $total, $currency, $crypto_selected_coin, $crypto_total, $invoice_id, $nonce, $crypto_coin_address, $address_in, $prefix_url ) {

        $data = array(
            'user_id' => $crypto_username,
            'amount' => $total,
            'currency' => $currency,
            'crypto_coin' => $crypto_selected_coin,
            'crypto_amount' => $crypto_total,
            'crypto_amount_remaining' => '',
            'status' => 'pending',
            'order_id' => $invoice_id,
            'order_procced_time' => date('Y-m-d H:i:s'),
            'payment_received_time' => '',
            'nonce' => $nonce,
            'coin_address' => $crypto_coin_address,
            'coin_address_in' => $address_in,
            'prefix_url' => $prefix_url,
        );

        $this->db->where('order_id', $invoice_id);

        $query = $this->db->get(DB_PREFIX .'crypto_withdraw');

        $results = $query->result_array();
        
        if( isset( $results ) && empty( $results ) && is_array( $results ) ){

            $this->db->insert(DB_PREFIX .'crypto_withdraw', $data);
        }

    }

}