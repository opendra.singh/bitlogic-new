<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	
	public function __construct() {
        parent::__construct();
    }

    /**
     *
     * count_users
     *
     * @return mixed
     *
     */
	
	public function count_users() {
		return $this->db->count_all_results(DB_PREFIX .'user');
	}

    /**
     *
     * count_users_this_week
     *
     * @return mixed
     *
     */

    public function count_users_this_week() {
        return $this->db->query("SELECT count(1) as count FROM `". DB_PREFIX ."user` WHERE date_registered > DATE_SUB(NOW(), INTERVAL 1 WEEK)")->row();
    }

    /**
     *
     * count_users_this_month
     *
     * @return mixed
     *
     */

    public function count_users_this_month() {
        return $this->db->query("SELECT count(1) as count FROM `". DB_PREFIX ."user` WHERE date_registered > DATE_SUB(NOW(), INTERVAL 1 MONTH)")->row();
    }

    /**
     *
     * count_users_this_year
     *
     * @return mixed
     *
     */

    public function count_users_this_year() {
        return $this->db->query("SELECT count(1) as count FROM `". DB_PREFIX ."user` WHERE date_registered > DATE_SUB(NOW(), INTERVAL 1 YEAR)")->row();
    }

    /**
     *
     * get_latest_members
     *
     * @return mixed
     *
     */

    public function get_latest_members() {
        $this->db->select('user_id, username, first_name, last_name, email')->from(DB_PREFIX .'user');
        $this->db->order_by("user_id", "asc");
        $this->db->limit(5);
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }


    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /*code for retrieve data for dashborad */
    public function get_dashboradchart($whereArray, $groupArray){
        $table = "awc_wise_beneficiary_log_new";
        $colmn = "sum(total_family) as totfamily, sum(total_member) as totmember,
        (sum(pregnant_count)+sum(lactating_count)+sum(child_count)+sum(adolence_count)) as totben, 
        (sum(p_updated)+sum(l_updated)+sum(c_updated)+sum(a_updated)) as totbenupdate, 
        sum(total_member_added) as totnewrecord, 
        (sum(p_added)+sum(l_added)+sum(c_added)+sum(a_added)) as totnewben,         
        count(awccode) as totawc, 
        sum(pregnant_count) as totpreg, sum(p_updated) as totpregupdate, 
        sum(lactating_count) as totlact, sum(l_updated) as totlactupdate,
        sum(child_count) as totchild, sum(c_updated) as totchildupdate,
        sum(adolence_count) as totadol, sum(a_updated) as totadolupdate";
        return $this->sql_helper->getResult($table, $colmn, $whereArray, NULL, $groupArray);
    }

    public function get_dashboradanccol($whereArray, $groupArray){
        $table = "immunizedvhnd_rpt";
        $colmn = "sum(total_delivered_anc1) as total_delivered_anc1, 
        sum(total_delivered_anc2) as total_delivered_anc2,
        sum(total_delivered_anc3) as total_delivered_anc3,
        sum(total_delivered_anc4) as total_delivered_anc4,
        sum(late_delivered_anc1) as late_delivered_anc1, 
        sum(late_delivered_anc2) as late_delivered_anc2,
        sum(late_delivered_anc3) as late_delivered_anc3,
        sum(late_delivered_anc4) as late_delivered_anc4,
        sum(dues_delivered_anc1) as dues_delivered_anc1, 
        sum(dues_delivered_anc2) as dues_delivered_anc2,
        sum(dues_delivered_anc3) as dues_delivered_anc3,
        sum(dues_delivered_anc4) as dues_delivered_anc4
        ";
        return $this->sql_helper->getResult($table, $colmn, $whereArray, NULL, $groupArray);
    }

    public function get_dashboradchildcol($whereArray, $groupArray){
        $table = "awc_wise_child_log";
        $colmn = "COALESCE(sum(totchild06),0) as totchild06, 
        COALESCE(sum(totchild63),0) as totchild63,
        COALESCE(sum(totchild36),0) as totchild36";
        return $this->sql_helper->getResult($table, $colmn, $whereArray, NULL, $groupArray);
    }

    public function get_dashboradvaccinationcol($whereArray, $groupArray){
        $table = "vaccinedelivery_rpt";
        $colmn = "sum(BCG1) as BCG1,
                    sum(DPT1) as DPT1,sum(DPT2) as DPT2,
                    sum(JE1) as JE1,sum(JE2) as JE2,
                    sum(Measles1) as Measles1,sum(Measles2) as Measles2,
                    sum(OPV1) as OPV1,sum(OPV2) as OPV2,sum(OPV3) as OPV3,sum(OPV4) as OPV4,sum(OPV5) as OPV5,
                    sum(Pentavalent1) as Pentavalent1,sum(Pentavalent2) as Pentavalent2,sum(Pentavalent3) as Pentavalent3,
                    sum(TT1) as TT1,sum(TT2) as TT2,
                    sum(VitaminA1) as VitaminA1,sum(VitaminA2) as VitaminA2,sum(VitaminA3) as VitaminA3,sum(VitaminA4) as VitaminA4,sum(VitaminA5) as VitaminA5,
                    sum(VitaminA6) as VitaminA6,sum(VitaminA7) as VitaminA7,sum(VitaminA8) as VitaminA8,sum(VitaminA9) as VitaminA9,
                    sum(DueBCG1) as DueBCG1,
                    sum(DueDPT1) as DueDPT1,sum(DueDPT2) as DueDPT2,
                    sum(DueJE1) as DueJE1,sum(DueJE2) as DueJE2,
                    sum(DueMeasles1) as DueMeasles1,sum(DueMeasles2) as DueMeasles2,
                    sum(DueOPV1) as DueOPV1,sum(DueOPV2) as DueOPV2,sum(DueOPV3) as DueOPV3,sum(DueOPV4) as DueOPV4,sum(DueOPV5) as DueOPV5,
                    sum(DuePentavalent1) as DuePentavalent1,sum(DuePentavalent2) as DuePentavalent2,sum(DuePentavalent3) as DuePentavalent3,
                    sum(DueTT1) as DueTT1,sum(DueTT2) as DueTT2,
                    sum(DueVitaminA1) as DueVitaminA1,sum(DueVitaminA2) as DueVitaminA2,sum(DueVitaminA3) as DueVitaminA3,sum(DueVitaminA4) as DueVitaminA4,sum(DueVitaminA5) as DueVitaminA5,
                    sum(DueVitaminA6) as DueVitaminA6,sum(DueVitaminA7) as DueVitaminA7,sum(DueVitaminA8) as DueVitaminA8,sum(DueVitaminA9) as DueVitaminA9,
                    sum(LateBCG1) as LateBCG1,
                    sum(LateDPT1) as LateDPT1,sum(LateDPT2) as LateDPT2,
                    sum(LateJE1) as LateJE1,sum(LateJE2) as LateJE2,
                    sum(LateMeasles1) as LateMeasles1,sum(LateMeasles2) as LateMeasles2,
                    sum(LateOPV1) as LateOPV1,sum(LateOPV2) as LateOPV2,sum(LateOPV3) as LateOPV3,sum(LateOPV4) as LateOPV4,sum(LateOPV5) as LateOPV5,
                    sum(LatePentavalent1) as LatePentavalent1,sum(LatePentavalent2) as LatePentavalent2,sum(LatePentavalent3) as LatePentavalent3,
                    sum(LateTT1) as LateTT1,sum(LateTT2) as LateTT2,
                    sum(LateVitaminA1) as LateVitaminA1,sum(LateVitaminA2) as LateVitaminA2,sum(LateVitaminA3) as LateVitaminA3,sum(LateVitaminA4) as LateVitaminA4,sum(LateVitaminA5) as LateVitaminA5,
                    sum(LateVitaminA6) as LateVitaminA6,sum(LateVitaminA7) as LateVitaminA7,sum(LateVitaminA8) as LateVitaminA8,sum(LateVitaminA9) as LateVitaminA9"; 

        return $this->sql_helper->getResult($table, $colmn, $whereArray, NULL, $groupArray);
    }
}