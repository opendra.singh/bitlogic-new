<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {

    public function __construct() {
        parent::__construct();

    }


   
    public function ret_awc($dist, $proj, $sect, $code) {
        $table = 'awc_master ';
        $columns = 'name';
        $where = array("distcode"=>$dist, "projectcode"=>$proj, "sectorcode"=>$sect,"code"=>$code);

        return $this->sql_helper->retText($table, $columns, $where);
    }

  

    public function get_dist_data() {
        $table = 'awc_wise_beneficiary_log_new ';
        $columns = 'distcode,distname as name,sum(total_member) as total_member , 
                    sum(total_member_added) as total_mem_added , sum(pregnant_count) as pregnant_count ,
                    sum(lactating_count) as lactating_count,sum(child_count) as child_count,
                    sum(adolence_count) as adolence_count, sum(child_mother_tagging) as mother_tagging,
                    sum(total_updated) as total_updated,
                    sum(p_added) as p_added,sum(l_added) as l_added,
                    sum(c_added) as c_added,sum(a_added) as a_added,
                    sum(p_updated) as p_updated,sum(p_new_updated) as p_new_updated,
                    sum(p_old_updated) as p_old_updated,
                    sum(pregnant_aadhar_na) as preg_aadhar_na,
                    sum(pregnant_lmp_date_na) as preg_lmp_na,
                    sum(pregnant_reg_date_na) as preg_reg_na,
                    sum(pregnant_ifa_count_na) as preg_ifa_na,
                    sum(l_updated) as l_updated,sum(l_new_updated) as l_new_updated,sum(l_old_updated) as l_old_updated,
                    sum(lactating_aadhar_na) as lact_aadhar_na,
                    sum(lactating_delivery_date__na) as lact_delivery_na,
                    sum(c_updated) as c_updated,sum(c_new_updated) as c_new_updated,sum(c_old_updated) as c_old_updated,
                    sum(child_aadhar_na) as child_aadhar_na,
                    sum(child_mother_tagging) as ch_aadhar_na,
                    sum(a_updated) as a_updated,sum(a_new_updated) as a_new_updated,sum(a_old_updated) as a_old_updated,
                    sum(adolence_aadhar_na) as adol_aadhar_na,
                    sum(child_mother_tagging) as mother_tagging,sum(female_immunization) as female_immunization,
                    sum(child_immunization) as child_immunization,';
        $order = array('distname'=>'asc');
        $groupby = array('distcode');
        return $this->sql_helper->getResult($table, $columns, null, $order, $groupby);
    }

    public function get_proj_data($dist) {
        $table = 'awc_wise_beneficiary_log_new ';
        $columns = 'distcode,distname,projectcode,projectname as name,sum(total_member) as total_member , 
                    sum(total_member_added) as total_mem_added , sum(pregnant_count) as pregnant_count ,
                    sum(lactating_count) as lactating_count,sum(child_count) as child_count,
                    sum(adolence_count) as adolence_count, sum(child_mother_tagging) as mother_tagging,
                    sum(total_updated) as total_updated,
                    sum(p_added) as p_added,sum(l_added) as l_added,
                    sum(c_added) as c_added,sum(a_added) as a_added,
                    sum(p_updated) as p_updated,sum(p_new_updated) as p_new_updated,
                    sum(p_old_updated) as p_old_updated,
                    sum(pregnant_aadhar_na) as preg_aadhar_na,
                    sum(pregnant_lmp_date_na) as preg_lmp_na,
                    sum(pregnant_reg_date_na) as preg_reg_na,
                    sum(pregnant_ifa_count_na) as preg_ifa_na,
                    sum(l_updated) as l_updated,sum(l_new_updated) as l_new_updated,sum(l_old_updated) as l_old_updated,
                    sum(lactating_aadhar_na) as lact_aadhar_na,
                    sum(lactating_delivery_date__na) as lact_delivery_na,
                    sum(c_updated) as c_updated,sum(c_new_updated) as c_new_updated,sum(c_old_updated) as c_old_updated,
                    sum(child_aadhar_na) as child_aadhar_na,
                    sum(child_mother_tagging) as ch_aadhar_na,
                    sum(a_updated) as a_updated,sum(a_new_updated) as a_new_updated,sum(a_old_updated) as a_old_updated,
                    sum(adolence_aadhar_na) as adol_aadhar_na,
                    sum(child_mother_tagging) as mother_tagging,sum(female_immunization) as female_immunization,
                    sum(child_immunization) as child_immunization,';
        $where = array('distcode'=>$dist);
        $groupby = array('projectcode');
        $order = array('projectname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }

    public function get_sect_data($dist, $proj) {
        $table = 'awc_wise_beneficiary_log_new ';
        $columns = 'distcode,distname,projectcode,projectname,sectorcode,sectorname as name,sum(total_member) as total_member , 
                    sum(total_member_added) as total_mem_added , sum(pregnant_count) as pregnant_count ,
                    sum(lactating_count) as lactating_count,sum(child_count) as child_count,
                    sum(adolence_count) as adolence_count, sum(child_mother_tagging) as mother_tagging,
                    sum(total_updated) as total_updated,
                    sum(p_added) as p_added,sum(l_added) as l_added,
                    sum(c_added) as c_added,sum(a_added) as a_added,
                    sum(p_updated) as p_updated,sum(p_new_updated) as p_new_updated,
                    sum(p_old_updated) as p_old_updated,
                    sum(pregnant_aadhar_na) as preg_aadhar_na,
                    sum(pregnant_lmp_date_na) as preg_lmp_na,
                    sum(pregnant_reg_date_na) as preg_reg_na,
                    sum(pregnant_ifa_count_na) as preg_ifa_na,
                    sum(l_updated) as l_updated,sum(l_new_updated) as l_new_updated,sum(l_old_updated) as l_old_updated,
                    sum(lactating_aadhar_na) as lact_aadhar_na,
                    sum(lactating_delivery_date__na) as lact_delivery_na,
                    sum(c_updated) as c_updated,sum(c_new_updated) as c_new_updated,sum(c_old_updated) as c_old_updated,
                    sum(child_aadhar_na) as child_aadhar_na,
                    sum(child_mother_tagging) as ch_aadhar_na,
                    sum(a_updated) as a_updated,sum(a_new_updated) as a_new_updated,sum(a_old_updated) as a_old_updated,
                    sum(adolence_aadhar_na) as adol_aadhar_na,
                    sum(child_mother_tagging) as mother_tagging,sum(female_immunization) as female_immunization,
                    sum(child_immunization) as child_immunization,';
        $where = array('distcode'=>$dist,'projectcode'=>$proj);
        $groupby = array('sectorcode');
        $order = array('sectorname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }

    public function get_awc_data($dist, $proj, $sect) {
        $table = 'awc_wise_beneficiary_log_new ';
        $columns = 'distcode,distname,projectcode,projectname,sectorcode,sectorname,awccode,awcname as name,sum(total_member) as total_member , 
                    sum(total_member_added) as total_mem_added , sum(pregnant_count) as pregnant_count ,
                    sum(lactating_count) as lactating_count,sum(child_count) as child_count,
                    sum(adolence_count) as adolence_count, sum(child_mother_tagging) as mother_tagging,
                    sum(total_updated) as total_updated,
                    sum(p_added) as p_added,sum(l_added) as l_added,
                    sum(c_added) as c_added,sum(a_added) as a_added,
                    sum(p_updated) as p_updated,sum(p_new_updated) as p_new_updated,
                    sum(p_old_updated) as p_old_updated,
                    sum(pregnant_aadhar_na) as preg_aadhar_na,
                    sum(pregnant_lmp_date_na) as preg_lmp_na,
                    sum(pregnant_reg_date_na) as preg_reg_na,
                    sum(pregnant_ifa_count_na) as preg_ifa_na,
                    sum(l_updated) as l_updated,sum(l_new_updated) as l_new_updated,sum(l_old_updated) as l_old_updated,
                    sum(lactating_aadhar_na) as lact_aadhar_na,
                    sum(lactating_delivery_date__na) as lact_delivery_na,
                    sum(c_updated) as c_updated,sum(c_new_updated) as c_new_updated,sum(c_old_updated) as c_old_updated,
                    sum(child_aadhar_na) as child_aadhar_na,
                    sum(child_mother_tagging) as ch_aadhar_na,
                    sum(a_updated) as a_updated,sum(a_new_updated) as a_new_updated,sum(a_old_updated) as a_old_updated,
                    sum(adolence_aadhar_na) as adol_aadhar_na,
                    sum(child_mother_tagging) as mother_tagging,sum(female_immunization) as female_immunization,
                    sum(child_immunization) as child_immunization,';
        $where = array('distcode'=>$dist,'projectcode'=>$proj,'sectorcode'=>$sect);
        $groupby = array('awccode');
        $order = array('awcname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }


    public function get_child_data() {
        $table = 'awc_wise_child_log';
        $columns = 'distcode,distname as name,sum(totchild) as totchild,sum(totchild06) as totchild06_child ,
                    sum(totchild06added) as totchild06_added , 
                    sum(totchild06update) as totchild06_update ,sum(totchild06imun) as totchild06_imun,
                    sum(totchild63) as totchild63_child ,sum(totchild63added) as totchild63_added , 
                    sum(totchild63update) as totchild63_update ,sum(totchild63imun) as totchild63_imun,                    
                    sum(totchild36) as totchild36_child ,sum(totchild36added) as totchild36_added , 
                    sum(totchild36update) as totchild36_update ,sum(totchild36imun) as totchild36_imun';
        $order = array('distname'=>'asc');
        $groupby = array('distcode');
        return $this->sql_helper->getResult($table, $columns, null, $order, $groupby);
    }

    public function get_proj_child_data($dist) {
        $table = 'awc_wise_child_log';
        $columns = 'distcode,distname,projectcode,projectname as name,sum(totchild) as totchild,
                    sum(totchild06) as totchild06_child ,sum(totchild06added) as totchild06_added , 
                    sum(totchild06update) as totchild06_update ,sum(totchild06imun) as totchild06_imun,
                    sum(totchild63) as totchild63_child ,sum(totchild63added) as totchild63_added , 
                    sum(totchild63update) as totchild63_update ,sum(totchild63imun) as totchild63_imun,                    
                    sum(totchild36) as totchild36_child ,sum(totchild36added) as totchild36_added , 
                    sum(totchild36update) as totchild36_update ,sum(totchild36imun) as totchild36_imun';
        $where = array('distcode'=>$dist);
        $groupby = array('projectcode');
        $order = array('projectname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }

    public function get_sect_child_data($dist, $proj) {
        $table = 'awc_wise_child_log';
        $columns = 'distcode,distname,projectcode,projectname,sectorcode,sectorname as name,sum(totchild) as totchild,
                    sum(totchild06) as totchild06_child , sum(totchild06added) as totchild06_added , sum(totchild06update) as totchild06_update ,sum(totchild06imun) as totchild06_imun,
                    sum(totchild63) as totchild63_child ,sum(totchild63added) as totchild63_added , 
                    sum(totchild63update) as totchild63_update ,sum(totchild63imun) as totchild63_imun,                    
                    sum(totchild36) as totchild36_child ,sum(totchild36added) as totchild36_added , 
                    sum(totchild36update) as totchild36_update ,sum(totchild36imun) as totchild36_imun';
        $where = array('distcode'=>$dist,'projectcode'=>$proj);
        $groupby = array('sectorcode');
        $order = array('sectorname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }

    public function get_awc_child_data($dist, $proj, $sect) {
        $table = 'awc_wise_child_log';
        $columns = 'distcode,distname,projectcode,projectname,sectorcode,sectorname,awccode,awcname as name,
                    sum(totchild) as totchild,sum(totchild06) as totchild06_child ,sum(totchild06added) as totchild06_added ,
                    sum(totchild06update) as totchild06_update ,sum(totchild06imun) as totchild06_imun,
                    sum(totchild63) as totchild63_child ,sum(totchild63added) as totchild63_added , 
                    sum(totchild63update) as totchild63_update ,sum(totchild63imun) as totchild63_imun,                    
                    sum(totchild36) as totchild36_child ,sum(totchild36added) as totchild36_added , 
                    sum(totchild36update) as totchild36_update ,sum(totchild36imun) as totchild36_imun';
        $where = array('distcode'=>$dist,'projectcode'=>$proj,'sectorcode'=>$sect);
        $groupby = array('awccode');
        $order = array('awcname'=>'asc');

        return $this->sql_helper->getResult($table, $columns, $where, $order, $groupby);
    }

    

    public function get_tot_benf($whr) {

        $this->db->distinct();
        $this->db->select("cfm.distcode as distcode,cfm.projectcode as projectcode ,cfm.sectorcode as sectorcode ,cfm.awccode as awccode,cfm.id as member_id,cfm.auto_code as auto_code,cfm.name , cfm.gender , cfm.dob,cfm.marital_status, IFNULL(f.name,'NA') as mother_name");
        $this->db->from('family_master cfm');
        $this->db->join("family_master f" , "f.id=cfm.mother_id",'left');
        $this->db->where($whr);
        $this->db->order_by('cfm.dob','asc');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array() ;
        }
        return FALSE;   

    }

    public function get_women_immun($whr) {

        $this->db->distinct();
        $this->db->select("cfm.distcode as distcode,cfm.projectcode as projectcode ,cfm.sectorcode as sectorcode ,cfm.awccode as awccode,cfm.id as member_id,cfm.auto_code as auto_code,cfm.name , cfm.gender , cfm.dob,cfm.marital_status, IFNULL(f.name,'NA') as mother_name");
        $this->db->from('womenanccheckup_details w');
        $this->db->join('family_master cfm' , 'cfm.id=w.member_id');
        $this->db->join("family_master f" , "f.id=cfm.mother_id",'left');
        $this->db->where($whr);
        $this->db->order_by('cfm.dob','asc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array() ;
        }
        return FALSE;        
    }

    public function get_child_immun($whr) {
        $this->db->distinct();
        $this->db->select("cfm.distcode as distcode,cfm.projectcode as projectcode ,cfm.sectorcode as sectorcode ,cfm.awccode as awccode,cfm.id as member_id,cfm.auto_code as auto_code,cfm.name , cfm.gender , cfm.dob,cfm.marital_status, IFNULL(f.name,'NA') as mother_name");
        $this->db->from('family_master cfm');
        $this->db->join('vhndvaccine_details c' , 'cfm.id=c.member_id');
        $this->db->join("family_master f" , "f.id=cfm.mother_id",'left');
        $this->db->where($whr);
        $this->db->order_by('cfm.dob','asc');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array() ;
        }
        return FALSE;
    }



    public function get_memberdata($distcode, $projectcode, $sectorcode, $awccode, $id) {
        $table = 'family_master';
        $columns = 'name,code,dob,category_code,target_code,is_residence';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, "id"=>$id);

        return $this->sql_helper->getResult($table, $columns, $where);
    }   

    public function get_vaccinedata($vid) {
        $table = 'immunization_master i';
        $columns = 'v.vacronym, i.doses_no, i.schedule_min, i.schedule_format';
        $where = array('i.vid'=>$vid);
        $join = array('vaccine_master v' => 'i.vid=v.id');
        return $this->sql_helper->getJoinResult($table, $columns, $join, $where);
    }  

     public function get_childimunizationdata($distcode, $projectcode, $sectorcode, $awccode, $member_id) {
        $table = 'vhndvaccine_details';
        $columns = 'vid,doses_no,due_date,delivery_date';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, "member_id"=>$member_id);
        $order = array('due_date'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $order);
    }  


     public function get_womenstatusdata($distcode, $projectcode, $sectorcode, $awccode, $member_id) {
        $table = 'womenstatus_master';
        $columns = 'anc1due,anc2due,anc3due,anc4due';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, "member_id"=>$member_id);

        return $this->sql_helper->getResult($table, $columns, $where);
    }  


     public function get_ancdata($distcode, $projectcode, $sectorcode, $awccode, $member_id) {
        $table = 'womenanccheckup_details';
        $columns = 'anc1delivery,anc2delivery,anc3delivery,anc4delivery,ifa_count';
        $where = array('distcode'=>$distcode, 'projectcode'=>$projectcode, 'sectorcode'=>$sectorcode, 'awccode'=>$awccode, "member_id"=>$member_id);

        $order = array('id'=>'desc');
        return $this->sql_helper->getResult($table, $columns, $where);
    }  

}

