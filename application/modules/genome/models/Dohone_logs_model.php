<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dohone_logs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_deposit_log( $username, $telephone, $email, $merchant_code, $uniqueTransactionId, $amount, $currency, $success, $notify, $cancel, $hash_uniq_code ) {

        $data = array(
            'user_name' => $username,
            'telephone' => $telephone,
            'email'     => $email,
            'merchant_code' => $merchant_code,
            'uniqueTransactionId' => $uniqueTransactionId,
            'amount' => $amount,
            'currency' => $currency,
            'status' => 'pending',
            'order_procced_time' => date('Y-m-d H:i:s'),
            'payment_received_time' => '',
            'success_url' => $success,
            'notify_url' => $notify,
            'hash_uniq_code' => $hash_uniq_code,
            'cancel_url' => $cancel,
            'log' => 'deposit',
            'r_txn_reference' => '',
            'r_payment_mode' => '',
            'r_hash_key' => '',
        );
        $this->db->insert(DB_PREFIX .'dohone_logs', $data);
    }

    public function verify_transaction($r_uniqueTransactionId) {

        $this->db->where('uniqueTransactionId', $r_uniqueTransactionId);

        $query = $this->db->get(DB_PREFIX .'dohone_logs');

        $results = $query->result_array();

        if( isset( $results ) && empty( $results ) && is_array( $results ) ) {
            return 'ok';
        }
            
    }

    public function validate_amount( $r_uniqueTransactionId, $r_amount_paid, $r_currency ) {

        $this->db->where('uniqueTransactionId', $r_uniqueTransactionId);

        $query = $this->db->get(DB_PREFIX .'dohone_logs');

        $results = $query->result_array();

        if( isset( $results ) && empty( $results ) && is_array( $results ) ) {
            if( ( $r_amount_paid >= $results[0]['amount'] ) && ( $results[0]['currency'] == $r_currency ) ) {
                return 'ok';
            }
        }

    }

    public function validate_merchant( $r_uniqueTransactionId, $r_merchant_code ) {

        $this->db->where('uniqueTransactionId', $r_uniqueTransactionId);

        $query = $this->db->get(DB_PREFIX .'dohone_logs');

        $results = $query->result_array();

        if( isset( $results ) && empty( $results ) && is_array( $results ) ) {
            if( $results[0]['merchant_code'] == $r_merchant_code ) {
                return 'ok';
            }
        }

    }

    public function save_notify_data($r_uniqueTransactionId, $r_txn_reference, $r_payment_mode, $r_hash_key) {

        $value = array('r_txn_reference' => $r_txn_reference, 'r_payment_mode' => $r_payment_mode, 'r_hash_key' => $r_hash_key);
        $this->db->where('uniqueTransactionId', $r_uniqueTransactionId);
        $this->db->update(DB_PREFIX .'dohone_logs', $value);
    }

    public function get_user_id($r_uniqueTransactionId) {

        $this->db->where('uniqueTransactionId', $r_uniqueTransactionId);

        $query = $this->db->get(DB_PREFIX .'dohone_logs');

        $results = $query->result_array();

        if( isset( $results ) && empty( $results ) && is_array( $results ) ) {
            if( isset($results[0]['user_name']) ) {
                return $results[0]['user_name'];   
            }
        }
            
    }

}