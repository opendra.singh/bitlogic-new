<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'vendor/autoload.php';
use AyubIRZ\PerfectMoneyAPI\PerfectMoneyAPI;

class Perfectmoney_controller extends Gateway_Controller {

    public $content_data;

    private $PMAccountID = '4014188';   
    private $PMPassPhrase = 'perfect123';   

    public function __construct()
    {
        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function getAccountName($account)
    {
        // trying to open URL to process PerfectMoney getAccountName request
        $this->PMAccountID = '4014188';
        $this->PMPassPhrase = 'Perfect123';
        //$data = file_get_contents("https://perfectmoney.com/acct/balance.asp?AccountID=".$this->PMAccountID."&PassPhrase=".$this->PMPassPhrase."&Account=".$account."", 'rb');

        $data = file_get_contents("https://perfectmoney.com/acct/confirm.asp?AccountID=".$this->PMAccountID."&PassPhrase=".$this->PMPassPhrase."&Payer_Account=U987654&Payee_Account=U1234567&Amount=1&PAY_IN=1&PAYMENT_ID=1223", 'rb');

        if($data == 'ERROR: Can not login with passed AccountID and PassPhrase'){

            throw new Exception('Invalid PerfectMoney Username or Password.', 500);

        }elseif($data == 'ERROR: Invalid Account'){

            throw new Exception('Invalid PerfectMoney Account specified.', 500);

        }

        return $data;
    }

    public function index() {

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Home', 'perfectmoney', 'header', 'footer', '', $this->content_data);
    }

    public function deposit()
    {
        $post = $this->input->post();

        //calculate signature
        $uniqueuserid = $post["username"];
        $uniqueTransactionId = $post["uniqueTransactionId"];
        $pmAccountID = "U23568901";

        $customproduct = array(
                    "currency"=>$post["currency"],
                    "amount"=>$post["amount"]
                  );

        $this->content_data["uniquetid"] = $uniqueTransactionId;
        $this->content_data["uniqueuserid"] = $uniqueuserid;
        $this->content_data["pmAccountID"] = $pmAccountID;
        $this->content_data["customproduct"] = $customproduct;

        $this->content_data["success_url"] = "http://localhost/bitlogic/gateway/perfectmoney/success";
        $this->content_data["decline_url"] = "http://localhost/bitlogic/gateway/perfectmoney/decline";
        $this->content_data["backUrl"] = "http://localhost/bitlogic/gateway/perfectmoney/cancel";

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'PM Checkout', 'perfectmoney_checkout', 'header', 'footer', '', $this->content_data);

    }

    public function success()
    {
        print_r($_REQUEST);
    }
    public function decline()
    {
       print_r($_REQUEST);
    }
    public function cancel()
    {
        print_r($_REQUEST);
    }

}