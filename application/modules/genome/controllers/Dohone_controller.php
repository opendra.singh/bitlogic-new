<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dohone_controller extends Gateway_Controller {

    public $content_data;
    public $merchant_code = 'GD216E19969793310088401';
    public $hash_uniq_code = '';
    public $hash_code_withdraw = '8A36DB3A56A8D27135B56FAB10AD12';
    public function __construct() {

        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('dohone_logs_model');
    }

    public function deposit() {
        $post = $this->input->post();
        $success = site_url('genome/dohone_controller/success');
        $notify = site_url('genome/dohone_controller/notify');
        $cancel = site_url('genome/dohone_controller/cancel');
        $this->content_data['username'] = $post["username"];
        $this->content_data['telephone'] = $post["telephone"];
        $this->content_data['email'] = $post["email"];
        $this->content_data['merchant_code'] = $this->merchant_code;
        $this->content_data['uniqueTransactionId'] = $post["uniqueTransactionId"];
        $this->content_data['amount'] = $post["amount"];
        $this->content_data['currency'] = $post["currency"];
        $this->content_data['endPage'] = $success;
        $this->content_data['notifyPage'] = $notify;
        $this->content_data['cancelPage'] = $cancel;
        $this->dohone_logs_model->insert_deposit_log( $post["username"], $post["telephone"], $post["email"], $this->merchant_code, $post["uniqueTransactionId"], $post["amount"], $post["currency"], $success, $notify, $cancel, $this->hash_uniq_code );
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Checkout', 'dohone_checkout', 'header', 'footer', '', $this->content_data);
    }

    public function success() {
        
        if( isset($_GET) && !empty($_GET) ) {
            $r_uniqueTransactionId = $_GET['rI'];
            $r_amount_paid = (float)$_GET['rMt'];
            $r_currency = $_GET['rDvs'];
            $r_txn_reference = $_GET['idReqDoh'];
            $r_merchant_code = $_GET['rH'];
            $r_payment_mode = $_GET['mode'];
            $r_hash_key = $_GET['hash'];

            if( $r_txn_reference ) {
                $params = "cmd=verify&rI={$r_uniqueTransactionId}&rMt={$r_amount_paid}&idReqDoh={$r_txn_reference}&rDvs={$r_currency}";
                $linkSource = "https://www.my-dohone.com/dohone/pay?".$params;
                $curl = curl_init ($linkSource);
                curl_setopt ($curl, CURLOPT_FAILONERROR, TRUE);
                curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                $result = curl_exec ($curl);
                if ($result == 'OK') {
                    $verified = $this->dohone_logs_model->verify_transaction($r_uniqueTransactionId);
                    if( $verified == 'ok' ) {
                        $amount_verified = $this->dohone_logs_model->validate_amount($r_uniqueTransactionId, $r_amount_paid, $r_currency);
                        if( $amount_verified == 'ok' ) {
                            $merchant_verified = $this->dohone_logs_model->validate_merchant($r_uniqueTransactionId, $r_merchant_code);
                            if( $merchant_verified == 'ok' ) {
                                $this->dohone_logs_model->save_notify_data($r_uniqueTransactionId, $r_txn_reference, $r_payment_mode, $r_hash_key);
                                $uniqueUserId = $this->dohone_logs_model->get_user_id($r_uniqueTransactionId);
                                $this->content_data["transactionId"] = $r_uniqueTransactionId;
                                $this->content_data["uniqueUserId"] = $uniqueUserId;
                                $this->content_data["totalAmount"] = $r_amount_paid;
                                $this->content_data["currency"] = $r_currency;
                                $this->content_data["message"] = 'Your Amount has been received and transferred to Merchant';
                                $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Success', 'success', 'header', 'footer', '', $this->content_data);
                            }
                        } else {
                            echo 'Amount is paid in wrong currency or Low amount paid';
                        }
                    } else {
                        echo 'Order or Transaction key does not Match';
                    }
                } else{
                    echo 'This Payment is not recognized by DOHONE';
                }
            }
        }
    }

    public function notify() {

        if( isset($_GET) && !empty($_GET) ) {
            $r_uniqueTransactionId = $_GET['rI'];
            $r_amount_paid = (float)$_GET['rMt'];
            $r_currency = $_GET['rDvs'];
            $r_txn_reference = $_GET['idReqDoh'];
            $r_merchant_code = $_GET['rH'];
            $r_payment_mode = $_GET['mode'];
            $r_hash_key = $_GET['hash'];

            if( $r_txn_reference ) {
                $params = "cmd=verify&rI={$r_uniqueTransactionId}&rMt={$r_amount_paid}&idReqDoh={$r_txn_reference}&rDvs={$r_currency}";
                $linkSource = "https://www.my-dohone.com/dohone/pay?".$params;
                $curl = curl_init ($linkSource);
                curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                $result = curl_exec ($curl);
                if ($result == 'OK') {
                    $verified = $this->dohone_logs_model->verify_transaction($r_uniqueTransactionId);
                    if( $verified == 'ok' ) {
                        $amount_verified = $this->dohone_logs_model->validate_amount($r_uniqueTransactionId, $r_amount_paid, $r_currency);
                        if( $amount_verified == 'ok' ) {
                            $merchant_verified = $this->dohone_logs_model->validate_merchant($r_uniqueTransactionId, $r_merchant_code);
                            if( $merchant_verified == 'ok' ) {
                                $this->dohone_logs_model->save_notify_data($r_uniqueTransactionId, $r_txn_reference, $r_payment_mode, $r_hash_key);
                            }
                        } else {
                            echo 'Amount is paid in wrong currency or Low amount paid';
                        }
                    } else {
                        echo 'Order or Transaction key does not Match';
                    }
                } else{
                    echo 'This Payment is not recognized by DOHONE';
                }
            }
        }
    }

    public function cancel() {
        $this->content_data["transactionId"] = '';
        $this->content_data["uniqueUserId"] = '';
        $this->content_data["totalAmount"] = '';
        $this->content_data["currency"] = '';
        $this->content_data["message"] = 'Either you have cancelled the payment, or server timeout';
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Cancelled', 'decline', 'header', 'footer', '', $this->content_data);
    }

    public function withdraw() {
        $post = $this->input->post();
        $this->content_data['username'] = $post["username"];
        $this->content_data['uniqueTransactionId'] = $post["uniqueTransactionId"];
        $this->content_data['currency'] = $post["currency"];
        $this->content_data['mobile'] = $post["mobile"];
        $this->content_data['city'] = $post["city"];
        $this->content_data['email'] = $post["email"];
        $this->content_data['country'] = $post["country"];
        $this->content_data['name'] = $post["name"];
        $this->content_data['amount'] = $post["amount"];
        $this->content_data['notify_url'] = site_url('genome/dohone_controller/notify_withdraw');
        $this->content_data['hash_uniq_code'] = $this->hash_code_withdraw;
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Checkout', 'dohone_withdraw_checkout', 'header', 'footer', '', $this->content_data);
    }

    public function notify_withdraw() {
        print_r('back'); die;
    }
}