<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Genome_controller extends Gateway_Controller {

    public $content_data;
    public function __construct()
    {
        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Home', 'test', 'header', 'footer', '', $this->content_data);
    }

    public function home() {

        $reqData = $this->input->get();
        $uniqueTransactionId = strtolower(date("dmYHis"));
        $this->content_data["uniqueTransactionId"] = $uniqueTransactionId;
        $this->content_data["username"] = isset($reqData["username"])? $reqData["username"]:"";
        $this->content_data["currency"] = isset($reqData["currency"])? $reqData["currency"]:"";
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Home', 'home', 'header', 'footer', '', $this->content_data);
    }

    public function deposit()
    {
        $post = $this->input->post();

        //calculate signature
        $key = "pkTest_c0IAIDHXDExfC6BQw1uU8NXHLIUeSdTV";
        $skTest = "skTest_7lTvywsJPM6dpo6gD1oIJqyXrJE69q0L";
        $uniqueuserid = $post["username"];
        $uniqueTransactionId = $post["uniqueTransactionId"];
        $customproduct = array(
                    "productId"=>$post["username"],
                    "productType"=>"fixedProduct",
                    "productName"=>$post["username"],
                    "currency"=>$post["currency"],
                    "amount"=>$post["amount"]
                  );

        $resulting = strtolower('backUrl=https://54.87.232.138/middleware_api/index.php/genome/cancel|buttontext=Pay!|customproduct=['. json_encode($customproduct) . ']|decline_url=https://54.87.232.138/middleware_api/index.php/genome/decline|height=auto|iframesrc=https://hpp-service.genome.eu/hpp|key='. $key . '|name=Payment Page|success_url=https://54.87.232.138/middleware_api/index.php/genome/success|type=integrated|uniqueTransactionId='. $uniqueTransactionId . '|uniqueuserid='. $uniqueuserid . '|width=auto|'. $skTest . '');
        
        $signature = hash("sha256", $resulting);

        $this->content_data["key"] = $key;
        $this->content_data["customproduct"] = $customproduct;
        $this->content_data["uniquetid"] = $uniqueTransactionId;
        $this->content_data["uniqueuserid"] = $uniqueuserid;
        $this->content_data["signature"] = $signature;

        $this->content_data["success_url"] = "https://54.87.232.138/middleware_api/index.php/genome/success";
        $this->content_data["decline_url"] = "https://54.87.232.138/middleware_api/index.php/genome/decline";
        $this->content_data["backUrl"] = "https://54.87.232.138/middleware_api/index.php/genome/cancel";

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Checkout', 'genome_checkout', 'header', 'footer', '', $this->content_data);
    }

    public function withdraw() 
    {
        $post = $this->input->post();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://gateway-sandbox.genome.eu/api/payout',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "api_version": 1,
            "method": "init",
            "merchant_account": "'. $post['merchant_account'] .'",
            "merchant_password": "'. $post['merchant_password'] .'",
            "transaction_unique_id": "PAYOUT_transaction_'.time().'",
            "amount": '. $post['amount'] .',
            "currency": "'. $post['currency'] .'",
            "callback_url": "'. $post['callback_url'] .'",
            "user_id": "'. $post['username'] .'",
            "user_ip": "'. $post['user_ip'] .'",
            "user_email": "'. $post['user_email'] .'",
            "card": {
                "card_cvv": "022",
                "card_number": "5191330000004415",
                "card_holder": "John Doe",
                "card_exp_year": "2020",
                "card_exp_month": "12"
                }
            }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function postCURL($_url, $_param){

        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }


    public function success()
    {
        $data = $_REQUEST;
        print_r($data);
        $this->content_data["transactionId"] = $data["transactionId"];
        $this->content_data["uniqueUserId"] = $data["uniqueUserId"];
        $this->content_data["totalAmount"] = $data["totalAmount"];
        $this->content_data["currency"] = $data["currency"];
        $this->content_data["message"] = $data["message"];
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Success', 'success', 'header', 'footer', '', $this->content_data);
    }
    public function decline()
    {
        $data = $_REQUEST;
        print_r($data);
        $this->content_data["transactionId"] = $data["transactionId"];
        $this->content_data["uniqueUserId"] = $data["uniqueUserId"];
        $this->content_data["totalAmount"] = $data["totalAmount"];
        $this->content_data["currency"] = $data["currency"];
        $this->content_data["message"] = $data["message"];
      $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Decline', 'decline', 'header', 'footer', '', $this->content_data);
    }
    public function cancel()
    {
      redirect("genome/cancel");
    }

}