<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_menu extends Admin_Controller {

    public function __construct()
    {   parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('add_menu_model');

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }
    }

    public function index() {

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }

        // get all menu
        $content_data['menus'] = $this->add_menu_model->get_menu();

        $this->template->set_metadata('description', 'Add menu');
        $this->template->set_breadcrumb('Add Menu', 'adminpanel/add_menu');

        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('add_menu'), 'add_menu', 'header', 'footer', '', $content_data);
    }


 

    /**
     *
     * add: add menu from post data.
     *
     */

    public function add_menu() {

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $this->form_validation->set_rules('menu_name', $this->lang->line('add_menu_name'), 'trim|required|max_length[100]|min_length[2]');

        /*$this->form_validation->set_rules('menu_url', $this->lang->line('add_menu_URL'), 'trim|required|max_length[100]|min_length[2]');*/

        /*$this->form_validation->set_rules('menu_position', $this->lang->line('add_menu_position'), 'trim|required|max_length[3]');*/

        $this->form_validation->set_rules('select_menu_is_parent', $this->lang->line('add_menu_is_parent'), 'trim|required|max_length[1]');

        $this->form_validation->set_rules('select_menu_has_parent', $this->lang->line('add_menu_has_parent'), 'trim|required|max_length[1]');

        $this->form_validation->set_rules('select_menu_active', $this->lang->line('add_menu_active'), 'trim|required|max_length[1]');

       
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata($_POST);
            redirect('/adminpanel/add_menu');
            exit();
        }

        $urlStr = $this->input->post('select_menu_is_parent');
        $checkUrl = $this->input->post('menu_url');

        if ($urlStr != 0) {
            $newUrl = '#';
        } else {            
            $newUrl = preg_replace('/([^:])(\/{2,})/', '$1/', $checkUrl);
            $parsed = parse_url($newUrl);

            if (is_numeric($newUrl)) {
            $newUrl = 'home/page/' .$newUrl;
            
            } else if (strpos($newUrl, 'home/page/') !== false) {
                 $newUrl = $newUrl;
            } else if (empty($parsed['scheme'])) {                
                $newUrl = 'http://'.$newUrl;
            }
        }
        

        $menu_parent_id = $this->input->post('menu_parent_id');
        if ($menu_parent_id == '') {
            $menu_parent_id = 0;
        }

        
        if($return_array = $this->add_menu_model->create_menu($this->input->post('menu_name'),$newUrl ,$this->input->post('select_menu_is_parent'),$menu_parent_id,$this->input->post('select_menu_active'))) {
            
            $this->session->set_flashdata('success', '<p>'. $this->lang->line('add_menu_success') .'</p>');
        }else{
            $this->session->set_flashdata('error', '<p>'. $this->lang->line('add_menu_unable') .'</p>');
        }
        redirect('/adminpanel/add_menu');
    }

     public function view_menu() {  

     if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        } 

        $this->template->set_metadata('description', 'View menu');
        $this->template->set_breadcrumb('View Menu', 'adminpanel/add_menu/view_menu');  

        //code to send parameter to datatable
        $tablename='dynamic_menu';
        $column='title, menu_id, url, position, parent_id, is_parent,show_menu';
        //$where="active=1";
        $order_by=null;
        $content_data['list_table'] = $this->add_menu_model->get_menu_data_list($tablename, $column, $order_by, null, 1000);

        //code to send parameter to page
        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', 'View Menu', 'view_menu_master', 'header', 'footer', '', $content_data);
    }




    public function edit_menu() {   

    if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }    

        $this->template->set_metadata('description', 'Edit menu');
        $this->template->set_breadcrumb('View Menu', 'adminpanel/add_menu/view_menu');

        //code to send parameter to datatable
        $id = $this->uri->segment(4);
        if ($id=='') {
           redirect('/adminpanel/add_menu/view_menu');
        }
        $tablename='dynamic_menu';
        $column='title, menu_id, url, position, parent_id, is_parent,show_menu';
        $where="menu_id=".$id;
        $order_by=null;
        $content_data['list_table'] = $this->add_menu_model->get_menu_data($tablename, $column, $where, $order_by, null, 1000);
        // get menu
        $content_data['menus'] = $this->add_menu_model->get_menu();
        //code to send parameter to page
        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', 'Edit Menu', 'edit_menu_master', 'header', 'footer', '', $content_data);
    }

 

    /**
     *
     * Update: Update menu from post data.
     *
     */

    public function update_menu() {

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        } 
        
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('menu_name', $this->lang->line('add_menu_name'), 'trim|required|max_length[100]|min_length[2]');
        /*$this->form_validation->set_rules('menu_url', $this->lang->line('add_menu_URL'), 'trim|required|max_length[100]|min_length[2]');
        $this->form_validation->set_rules('menu_position', $this->lang->line('add_menu_position'), 'trim|required|max_length[3]');*/
        /*$this->form_validation->set_rules('menu_parent_id', $this->lang->line('add_menu_parent'), 'trim|required|max_length[3]');*/
        $this->form_validation->set_rules('select_menu_is_parent', $this->lang->line('add_menu_is_parent'), 'trim|required|max_length[3]');

        $this->form_validation->set_rules('select_menu_active', $this->lang->line('add_menu_active'), 'trim|required|max_length[1]');

      
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata($_POST);
            redirect('/adminpanel/add_menu/view_menu');
            exit();
        }

        $menu_parent_id = $this->input->post('menu_parent_id');
        if ($menu_parent_id == '') {
            $menu_parent_id = 0;
        }

        $urlStr = $this->input->post('select_menu_is_parent');
        $checkUrl = $this->input->post('menu_url');

        if ($urlStr != 0) {
            $newUrl = '#';
        } else {            
            $newUrl = preg_replace('/([^:])(\/{2,})/', '$1/', $checkUrl);
            $parsed = parse_url($newUrl);

            if (is_numeric($newUrl)) {
            $newUrl = 'home/page/' .$newUrl;
            
            } else if (strpos($newUrl, 'home/page/') !== false) {
                 $newUrl = $newUrl;
            } else if (empty($parsed['scheme'])) {                
                $newUrl = 'http://'.$newUrl;
            }
        }


        if($return_array = $this->add_menu_model->update_menu($this->input->post('menu_id'),$this->input->post('menu_name'),$newUrl,$this->input->post('select_menu_is_parent'), $menu_parent_id, $this->input->post('select_menu_active'))) {
            
            $this->session->set_flashdata('success', '<p>'. $this->lang->line('add_menu_success') .'</p>');
        }else{
            $this->session->set_flashdata('error', '<p>'. $this->lang->line('add_menu_unable') .'</p>');
        }
        redirect('/adminpanel/add_menu/view_menu');
    }

}