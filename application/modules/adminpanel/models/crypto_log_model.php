<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crypto_Log_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * get_members: get the members data
     *
     * @param int $limit db limit (members per page)
     * @param int $offset db offset (current page)
     * @param string $order_by db sort order
     * @param string $sort_order asc or desc
     * @param array $search_data search input
     * @return mixed
     *
     */

    public function deposit($limit = 0, $offset = 0, $order_by = "user_id", $sort_order = "asc") {
        $this->db->select('*')->from(DB_PREFIX .'crypto_deposit');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    public function withdraw($limit = 0, $offset = 0, $order_by = "user_id", $sort_order = "asc") {
        $this->db->select('*')->from(DB_PREFIX .'crypto_withdraw');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }
    
    public function pay( $data_id ) {

        $this->db->select('*')->from(DB_PREFIX .'crypto_withdraw');
        $this->db->where('id', $data_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    public function deposit_ret( $data_id ) {

        $this->db->select('*')->from(DB_PREFIX .'crypto_deposit');
        $this->db->where('id', $data_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }
}