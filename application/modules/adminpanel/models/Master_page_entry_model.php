<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_page_entry_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        }



    /**
     *
     * get_notice_typ
     *
     * @return mixed
     *
     */

    public function get_notice_typ() {
        $this->db->select('typ_id as notice_id, typ_name as notice_name')->from(DB_PREFIX .'notification_typ');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }
    /**
     *
     * get_upload_typ
     *
     * @return mixed
     *
     */

    public function get_upload_typ() {
        $this->db->select('upload_type_id as upload_typ_id, upload_name as upload_typ_name')->from(DB_PREFIX .'upload_typ');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    /**
     *
     * create_page
     *
     * @param string $page_title
     * @param string $page_header
     * @param string $page_content
     * @return mixed
     *
     */

    public function create_page($data = array()) {

        $this->db->trans_start();

        $this->db->insert(DB_PREFIX .'master_page_entry', $data);

        $this->db->trans_complete();

        if (! $this->db->trans_status() === false)
        {
            return true;
        }

        return false;
    }



    /**
     *
     * get_page_data
     *
     * @param string $tablename ,$column,$where, $order_by=null 
     * @param string $sort_order = "asc", $limit = 0, $offset = 0 
     * @return mixed
     *
     */

   /* Code for retreieve data in list table */
    public function get_page_data($tablename, $column, $where, $order_by=null, $sort_order = "asc", $limit = 0, $offset = 0)
    {
        $fields = $this->db->list_fields(DB_PREFIX .$tablename);

        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        if(isset($where))
            $this->db->where($where);

        if(isset($order_by))
            $this->db->order_by($order_by, $sort_order);

        $this->db->limit($limit, $offset);

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return false;
    }


}

