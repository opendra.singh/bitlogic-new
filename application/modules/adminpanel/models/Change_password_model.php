<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_password_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    

    /**
     *
     * change_password: Update user password
     *
     * @param string $password
     * @return bool
     *
     */

    public function update_pass($password) {


        $this->db->trans_start();
        $this->db->where('user_id', $this->session->userdata('user_id'))->update(DB_PREFIX .'user', array('password' => password_hash(hash("sha256", $password), PASSWORD_DEFAULT)));
       
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return false;
        }

        return true;
    }

    
}