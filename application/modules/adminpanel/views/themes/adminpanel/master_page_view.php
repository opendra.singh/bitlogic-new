<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>


<?php $this->load->view('generic/flash_error'); ?>


<?php
if ($data) {

	$p_id = $data['p_id']; 
	$p_title = $data['p_title'];
	$p_header = $data['p_header'];
	$p_content = $data['p_content'];
}

?>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="menu_name"><?php print $this->lang->line('add_menu_name'); ?></label>
			<input type="text" name="menu_name" id="menu_name" class="form-control" 
			placeholder = "<?php print $this->lang->line('add_menu_name'); ?>" 
			value="<?php print $p_title; ?>"
			data-parsley-minlength="5"
			required>
		</div>

		<div class="form-group">
			<label for="menu_url"><?php print $this->lang->line('add_menu_URL'); ?></label>
			<input type="text" name="menu_url" id="menu_url" class="form-control"
			placeholder = "<?php print $this->lang->line('add_menu_URL'); ?>"       
			value="<?php print $p_header; ?>"
			data-parsley-minlength="5"
			required>
		</div>

		<div class="form-group">
			<label for="menu_position"><?php print $this->lang->line('add_menu_position'); ?></label>
			<input type="text" name="menu_position" id="menu_position" class="form-control"
			placeholder = "<?php print $this->lang->line('add_menu_position'); ?>" 
			value="<?php print $p_content; ?>"
			data-parsley-minlength="4"
			required>
		</div>

	</div>




</div>

<script src="<?php echo base_url(); ?>assets/js/validate/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/validate/additional-methods.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		/*$('.menu_parent').html('');*/

		var div_val = '<div class="form-group"> <label><?php print $this->lang->line('add_menu_parent'); ?></label>  <select class="form-control" name="menu_parent_id" id="menu_parent_id"><option value="">--Select Menu--</option><?php foreach($menus as $menu) {?> <option <?php if ($parent_id==$menu->menu_id) {  print 'selected="selected"';     }?> value="<?php print $menu->menu_id; ?>"><?php print $menu->menu_name; ?></option><?php } ?>   </select> </div>';

		$('#select_menu_is_parent').change(function(){
			$('.menu_parent').html('');
			if($('#select_menu_is_parent').val() == '1') {

				$('.menu_parent').append(div_val);
				$('.menu_parent').prop('required',true);
			} else {
				$('.menu_parent').html('');
				$('.menu_parent').prop('required',true);
			}
		});
	});

	$("#update_menu").validate({
		ignore:[],
		rules: {

			select_menu_active:{
				required: true
			},
			select_menu_is_parent:{
				required: true
			},
			menu_parent_id:{
				required: true
			}

		},
		messages: {

			select_menu_active:{
				required: " Please select activation"
			},
			select_menu_is_parent:{
				required: " Please select YES or NO"
			},
			menu_parent_id:{
				required: " Please select a Menu"
			}
		}
	});
</script>
