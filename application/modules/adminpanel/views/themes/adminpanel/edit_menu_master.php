<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>
<?php foreach($list_table->result() as $l_table) { 
  $menu_id = $l_table->menu_id; 
  $title = $l_table->title;
  $url = $l_table->url;
  $position = $l_table->position;
  $parent_id = $l_table->parent_id;
  $is_parent = $l_table->is_parent;      
  $show_menu = $l_table->show_menu;

}?> 
<?php $this->load->view('generic/flash_error'); ?>

<?php print form_open('adminpanel/add_menu/update_menu', array('id' => 'add_menu_form','autocomplete' => "off", 'class' => 'js-parsley', 'data-parsley-submit' => 'add_menu_submit')) ."\r\n"; ?>

<div class="row">
  <div class="col-sm-6">


    <div class="form-group">
    <label><?php print $this->lang->line('add_menu_is_parent'); ?></label>
    <select class="form-control" name= "select_menu_is_parent" id="select_menu_is_parent" required="required">
      <?php if ($is_parent == 1) { ?>
      <option value="">----Select Menu Is Parent----</option>
      <option selected="selected" value="1"> YES </option>
      <option value="0"> NO </option>
      <?php  } else if($is_parent == 0) { ?>

      <option value="">--Select Menu Is Parent--</option>
      <option value="1"> YES </option>
      <option selected="selected" value="0"> NO </option>

      <?php } else { ?>

      <option value="">--Select Menu Is Parent--</option>
      <option value="1"> YES </option>
      <option value="0"> NO </option>

      <?php } ?>

    </select>
  </div>   

  <div class="show_menu_url">

 </div>
   


    <div class="form-group">
      <label for="menu_name"><?php print $this->lang->line('add_menu_name'); ?></label>
      <input type="text" required="required" name="menu_name" id="menu_name" class="form-control" 
     placeholder = "<?php print $this->lang->line('add_menu_name'); ?>" 
     value="<?php print $title; ?>"
     data-parsley-maxlength="100"
     required>
   </div>


  <!-- <div class="form-group">
    <label for="menu_position"><?php print $this->lang->line('add_menu_position'); ?></label>
    <input type="text" name="menu_position" required="required" id="menu_position" class="form-control"
    placeholder = "<?php print $this->lang->line('add_menu_position'); ?>" 
    value="<?php print $position; ?>"
    data-parsley-maxlength="3"
    required>
  </div> -->


</div>

<div class="col-sm-6">


  <div class="form-group">
    <label><?php print $this->lang->line('add_menu_has_parent'); ?></label>
      <select class="form-control" name= "select_menu_has_parent" id="select_menu_has_parent" required="required">
        <?php if ($parent_id == 0) { ?>
        <option value="">----Select Menu Is Parent----</option>
        <option value="1"> YES </option>
        <option selected="selected" value="0"> NO </option>
        <?php  } else if($parent_id > 0) { ?>

        <option value="">--Select Menu Is Parent--</option>
        <option selected="selected" value="1"> YES </option>
        <option  value="0"> NO </option>

        <?php } else { ?>

        <option value="">--Select Menu Is Parent--</option>
        <option value="1"> YES </option>
        <option value="0"> NO </option>

        <?php } ?>

      </select>
 </div>

  <div class="menu_parent">
   <?php if (!$parent_id==0) { ?>
   
   <div class="form-group">
    <label><?php print $this->lang->line('add_menu_parent'); ?></label> 
    <select class="form-control" name="menu_parent_id" id="menu_parent_id" required="required">
     <option value="">--Select Menu--</option>
     <?php foreach($menus as $menu) {?>

     <option <?php if ($parent_id==$menu->menu_id) {
      echo 'selected="selected"';
    } ?> 
    value="<?php print $menu->menu_id; ?>"><?php print $menu->menu_name; ?></option><?php } ?>   </select>
  </div>

  
  <?php } ?>
  
</div>


</div>

<div class="col-sm-6">

  <div class="form-group">
    <label><?php print $this->lang->line('add_menu_active'); ?></label>
    <select class="form-control" name= "select_menu_active" id="select_menu_active" required="required">
      <?php if ($show_menu == 1) { ?>
      <option value="">--Select Activation--</option>
      <option selected="selected" value="1"> Activate </option>
      <option value="0"> De-Activate </option>
      <?php  } else if($show_menu == 0) { ?>

      <option value="">--Select Activation--</option>
      <option value="1"> Activate </option>
      <option selected="selected" value="0"> De-Activate </option>

      <?php } else { ?>

      <option value="">--Select Activation--</option>
      <option value="1"> Activate </option>
      <option value="0"> De-Activate </option>

      <?php } ?>

    </select>
  </div>
</div>

<?php echo form_hidden('menu_id',$menu_id);?>


<div class="col-sm-12">
  <div class="form-group">
    <button type="submit" name="add_menu_submit" id="add_menu_submit" class="add_menu_submit btn btn-primary btn-lg" data-loading-text="<?php print $this->lang->line('update_menu_loading_text'); ?>"><i class="fa fa-user-plus pd-r-5"></i> <?php print $this->lang->line('update_menu'); ?></button>
  </div>
</div>
</div>
<?php print form_close() ."\r\n"; ?>

<script src="<?php echo base_url(); ?>assets/js/validate/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/validate/additional-methods.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    /*$('.menu_parent').html('');*/

    var div_val = '<div class="form-group"> <label><?php print $this->lang->line('add_menu_parent'); ?></label>  <select class="form-control" required="required" name="menu_parent_id" id="menu_parent_id"><option value="">--Select Menu--</option><?php foreach($menus as $menu) {?> <option <?php if ($parent_id==$menu->menu_id) {  print 'selected="selected"';     }?> value="<?php print $menu->menu_id; ?>"><?php print $menu->menu_name; ?></option><?php } ?>   </select> </div>';

    $('#select_menu_has_parent').change(function(){
      $('.menu_parent').html('');
      if($('#select_menu_has_parent').val() == '1' ) {

        $('.menu_parent').append(div_val);
        $('.menu_parent').prop('required',true);
      } else {
       $('.menu_parent').html('');
       $('.menu_parent').prop('required',true);
     }
   });



        var div_url = '<div class="form-group">   <label for="menu_url"><?php print $this->lang->line('add_menu_URL'); ?></label>   <input type="text" required="required" name="menu_url" id="menu_url" class="form-control" placeholder = "<?php print $this->lang->line('add_menu_URL'); ?>" value="<?php print $url; ?>"data-parsley-maxlength="100"required>  </div>';

        var is_parent_val = '<?php echo $is_parent ?>';
        if (is_parent_val==0) {
            $('.show_menu_url').append(div_url);
          $('.show_menu_url').prop('required',true);
          }


    $('#select_menu_is_parent').change(function(){
      $('.show_menu_url').html('');
      if($('#select_menu_is_parent').val() == '0' ) {

        $('.show_menu_url').append(div_url);
        $('.show_menu_url').prop('required',true);
      } else {
       $('.show_menu_url').html('');
       $('.show_menu_url').prop('required',true);
     }
   });



  });

/*  $("#update_menu").validate({
    ignore:[],
    rules: {

     select_menu_active:{
       required: true
     },
     select_menu_is_parent:{
       required: true
     },
      select_menu_has_parent:{
       required: true
     },
     menu_parent_id:{
       required: true
     }

   },
   messages: {

    select_menu_active:{
      required: " Please select menu activation"
    },
    select_menu_is_parent:{
      required: " Please select YES or NO"
    },
    select_menu_has_parent:{
      required: " Please select YES or NO"
    },
    menu_parent_id:{
      required: " Please select a Menu"
    }
  }
});*/
</script>
