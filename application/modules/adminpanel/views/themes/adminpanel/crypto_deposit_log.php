<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Deposit Log </h2>

<table>
    <tr>
        <th>User Id</th>
        <th>Amount</th>
        <th>Amount in Crypto</th>
        <th>Payment Coin</th>
        <th>Transaction ID</th>
        <th>Status</th>
        <th>Amount Remaining(If Partial)</th>
        <th>Order Processed on</th>
        <th>Paymend made on</th>
        <th>Check Status</th>
    </tr>
    <?php
      if( isset($data) && is_array($data) && !empty($data) ) {
        foreach( $data as $val ) { ?>
          <tr>
              <td><?php echo $val->user_id ;?></td>
              <td><?php echo $val->currency . $val->amount; ?></td>
              <td><?php echo $val->crypto_amount ?></td>
              <td><?php echo $val->crypto_coin ?></td>
              <td><?php echo $val->order_id ?></td>
              <td><?php echo $val->status ?></td>
              <td><?php echo $val->crypto_amount_remaining ?></td>
              <td><?php echo $val->order_procced_time ?></td>
              <td><?php echo $val->payment_received_time ?></td>
              <td><?php echo "<button data-id='".$val->id."' class='reload_page'>Refresh</button>"; ?></td>
                  
          </tr>
          <?php
          }
      } ?>
</table>
<script>
  $(document).ready(function(){
    $(document).on('click','.reload_page',function(){
      var id = $(this).attr('data-id');
      var deposit = 'deposit';
        $.ajax({
          url   : '<?php echo site_url('adminpanel/crypto_log/update_status')?>',
          data: {id: id, deposit: deposit},
          dataType : 'json',
          success : function(data){
            window.location.reload();
          }
      });
    })
  })
</script>