<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('choose_roles.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>

<script>
  function toggle(source) {
        checkboxes = document.getElementsByName('chkflist[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            if(checkboxes[i].disabled==false)
            {
                checkboxes[i].checked = source.checked;
            }
        }
    }

    function opendetails(e, awccode)
    {
        if(e!="")
        {
          var test_token = '<?php echo $this->security->get_csrf_hash();?>';
            $.ajax({ 
                type: 'POST',
                url: '<?php print base_url(); ?>master/open_model',
                data: { csrf_token_name: test_token, eval: e, awccode: awccode },
                cache: false,
                success: function(data, status)
                {
                  var alert_string = "";
                  var data_arr = data.result.split("\\n");
                  for (var i = 0; i < data_arr.length; i++) {
                      alert_string += data_arr[i] + "\n";
                  }
                  alert(alert_string);
                },
                 async: true,
                 dataType: 'json'
           });
        }
    }

</script>
<div class="panel card bd-0">
    <div class="panel-body bg-white">


     <?php if($uri_segment!=NULL) { ?>

        <fieldset>
            <legend>Family List</legend>

             <?php print form_open('master/delete_family', array('id' => 'delete_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'delete_submit')) . "\r\n"; ?>
            <p style="border-bottom: solid 1px black;">*Click on row to see the member details of the selected family.</p>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                      <tr>
                          <th> 
                            <input type="checkbox" name="selectall" id="selectall"  onClick="toggle(this)" />
                          </th>
                          <th>Family Code</th>
                          <th>Member Name</th>
                          <th>Status</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=0;
                    if($familylist!=NULL && isset($familylist))
                    {
                        foreach($familylist->result() as $l_table) { ?>
                        <tr onclick="opendetails('<?php print $l_table->auto_code; ?>', '<?php print $l_table->awccode; ?>')" title="Click in rows to show family details.">
                            <td>
                                <input type="checkbox" name="chkflist[]" value="<?php echo $l_table->auto_code; ?>">
                            </td>
                            <td><?php print $l_table->code; ?></td>
                            <td><?php print strtoupper(mb_convert_encoding($l_table->name, 'Windows-1252', 'UTF-8')); ?></td>
                            <td><?php if($global->check_imunization($l_table->code, $l_table->awccode)==1) { print "<span style='color:red'>Can't delete due to imunization.</span>"; } else { print "<span style='color:green'>Available</span>"; } ?></td>
                        </tr>
                        <?php }} else { ?>
                        <tr>
                            <td colspan="4">No Records Found.</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <hr/>
        <div class="box-footer">
            <div class="col-md-12 col-sm-7" style="text-align: center;">
                 <input type="hidden" name="awccode" id="awccode" value="<?php echo $uri_segment; ?>"/> 
                <?php echo form_submit('delete_submit', 'Delete Records', 'id="delete_submit" class="btn btn-danger" onclick="this.value=\'Wait...\'"'); ?>
            </div>
        </div>

          <?php print form_close() . "\r\n"; ?> 
        </fieldset>

    <?php } else { ?>
            <div class="alert alert-warning alert-dismissible">
                <h4><i class="icon fa fa-warning"></i> Please select anganbari before continue.</h4>
            </div>
    <?php } ?>

    </div>
</div>


 <div  id='eachTable'></div>

