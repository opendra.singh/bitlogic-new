<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Site_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
         date_default_timezone_set("Asia/Kolkata");
        $this->load->library('form_validation');
        $this->load->model('master_page_entry_model');
    }

    public function index() {

        redirect("/");

        $tablename='master_page_entry';
        $column='page_upload_type,page_notice_type,page_id, page_title, page_header, page_content,
        CONVERT(page_file USING utf8) as file , page_file_name, page_start_date,
        page_end_date';
        $where="page_active = 1";
        $order_by=null;
        $result_data = $this->master_page_entry_model->get_page_data($tablename, $column,$where, $order_by, null, 1000);

        $page_data = array();       


        if ($result_data)
        {
            $i=0;
            foreach ($result_data->result() as $row => $innerArray)
            {
                $i=$i+1;

                
                $page_data[$i]['p_id']           = $innerArray->page_id;
                $page_data[$i]['p_title']        = $innerArray->page_title;
                $page_data[$i]['p_header']       = $innerArray->page_header;
                $page_data[$i]['p_content']      = $innerArray->page_content;
                $page_data[$i]['p_file_name']    = $innerArray->page_file_name;
                $page_data[$i]['p_file']         = $innerArray->file;
                $page_data[$i]['p_notice_typ']   = $innerArray->page_notice_type;
                $page_data[$i]['upload_type']    = $innerArray->page_upload_type;
                $page_data[$i]['p_st_date']      = $innerArray->page_start_date;
                $page_data[$i]['p_end_date']     = $innerArray->page_end_date;
            }


            $content_data['data'] =  $page_data;

        } else {
            $p_title = 'Page Under Construction';        
            $content_data['data']='';
        }

        $this->quick_page_setup(Settings_model::$db_config['active_theme'], 'main', 'Home', 'home', 'header', 'footer','', $content_data);
    }

   public function page() {  

        /*if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }   */  

        //code to send parameter to datatable
        $id = $this->uri->segment(3);
        $tablename='master_page_entry';
        $column='page_upload_type,page_id, page_title, page_header, page_content,
        CONVERT(page_file USING utf8) as file , page_file_name';
        $where="page_active = 1 AND page_menu_id=".$id;
        $order_by=null;
        $result_data = $this->master_page_entry_model->get_page_data($tablename, $column,$where, $order_by, null, 1000);

        $page_data = array();       

        $header1 =0;
        $header2 =0;
        $header3 =0;
        $header4 =0;

        if ($result_data)
        {
            $i=0;
            foreach ($result_data->result() as $row => $innerArray)
            {
                $i=$i+1;
                if ($innerArray->page_upload_type==1) {
                   $header1 =1;                   
                    
                } else if ($innerArray->page_upload_type==2) {
                    $header2 =1;                   
                    
                }else if ($innerArray->page_upload_type==3) {
                    $header3 =1;                   
                  
                }else if ($innerArray->page_upload_type==4) {
                    $header4 =1;                   
                   
                }
                $page_data['header1']    = $header1 ;
                $page_data['header2']    = $header2 ;
                $page_data['header3']    = $header3 ;
                $page_data['header4']    = $header4 ;


                $page_data[$i]['p_id']           = $innerArray->page_id;
                $page_data[$i]['p_title']        = $innerArray->page_title;
                $page_data[$i]['p_header']       = $innerArray->page_header;
                $page_data[$i]['p_content']      = $innerArray->page_content;
                $page_data[$i]['p_file_name']    = $innerArray->page_file_name;
                $page_data[$i]['p_file']         = $innerArray->file;
                $page_data[$i]['upload_type']    = $innerArray->page_upload_type;
            }


            $content_data['data'] =  $page_data;

        } else {
            $p_title = 'Page Under Construction';        
            $content_data['data']='';
        }
          //code to send parameter to page
        $this->quick_page_setup(Settings_model::$db_config['active_theme'], 'main', 'page_Title', 'master_page_view', 'header', 'footer', '', $content_data);
    }


    function view_content($value= FALSE)
    {
        if (!$value) {
            return FALSE;
        }

        $result = $this->master_page_entry_model->get_uploaded_document_content($value);

        if ($result) {
            echo '<embed width="100%" height="100%" alt="Responsive image" title="wcd" src="data:application/pdf;base64,'.$result->file.'"/>';

        }else{
            redirect('dashboard/index', 'refresh'); 
        }
    }

    /*
    * Function to view image in new tab which is in Gallery from dynamic pages
    *
    */

    function viewImage ($value= FALSE)
    {
      $output_file = 'tmp.jpg';
      $result = $this->master_page_entry_model->get_uploaded_img($value);
      
      echo '<img  src="data:image;base64,'.$result->file.'"/ style="padding: 10px;">';
     
    }

}