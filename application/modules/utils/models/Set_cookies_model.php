<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Set_cookies_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * load_session_vars: load the session variables from the DB
     *
     * @param string $cookie_part
     * @return mixed
     *
     */

    public function load_session_vars($cookie_part) {
        $this->db->select('u.user_id, u.username, ur.role_id, u.active, u.banned, u.last_login');
        $this->db->from(DB_PREFIX .'user u');
        $this->db->join(DB_PREFIX .'user_role ur', 'u.user_id = ur.user_id');
        $this->db->join(DB_PREFIX .'user_cookie_part ucp', 'u.user_id = ucp.user_id');
        $this->db->where('ucp.cookie_part', $cookie_part);
        $this->db->where('ucp.ip_address', $this->input->ip_address());
        $this->db->limit(1);

        $query = $this->db->get();

        $this->db->last_query();

        if($query->num_rows() == 1) {
            return $query->row();
        }
        return false;
    }
}

