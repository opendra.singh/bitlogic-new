<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class APIchild extends Rest_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['default_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['genomedeposit_post']['limit'] = 100; // 100 requests per hour per user/key
        //$this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function default_get()
    {
        //$this->load->model('api_model');
        
        $record = array (
            "message" => "Only authorized person allowed."
        );

        if (!empty($record))
        {
            $this->set_response($record, REST_Controller::HTTP_OK, "default"); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response(array('status' => FALSE,'message' => 'Data could not be found'), REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function genomedeposit_post()
    {
        $card = [
                'card_cvv'=> '022',
                'card_number'=> '5191330000004415',
                'card_holder'=> 'John Doe',
                'card_exp_year'=> '2020',
                'card_exp_month'=> '12'
                ];
        $message = [
            'api_version' => 1,
            'method' => 'init',
            'merchant_account' => 'testAccountFor_merchant_51768',
            'merchant_password'=> 'CrE+ud$fri+RU-5mU6*o',
            'transaction_unique_id'=> 'PAYOUT_transaction_'.time(),
            'amount'=> 10,
            'currency'=> 'EUR',
            'callback_url'=> 'https://merchant.com/callback',
            'user_id'=> 'user123',
            'user_ip'=> '193.34.96.64',
            'user_email'=> 'john.doe@example.com',
            'card'=> $card
        ];

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

/*
    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }
 */
}
