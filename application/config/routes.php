<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth/login';
$route['404_override'] = 'utils/page_not_found';
$route['translate_uri_dashes'] = FALSE;

/* Default router */
$route['login'] = 'auth/login';
$route['login/(.+)'] = 'auth/login/$1';
$route['logout'] = 'auth/logout';
$route['auth'] = 'auth/login';

//master form
$route['master'] = 'adminpanel/dashboard';


//payment loading page forr deposit and withdraw
$route['deposit/test'] = 'genome/dashboard/test_deposit';
$route['withdraw/test'] = 'genome/dashboard/test_withdraw';

$route['deposit/dashboard'] = 'genome/dashboard/deposit';
$route['withdraw/dashboard'] = 'genome/dashboard/withdraw';

/*genome router */
$route['genome/home'] = 'genome/genome_controller/home';
$route['genome/deposit'] = 'genome/genome_controller/deposit';
$route['genome/withdraw'] = 'genome/genome_controller/withdraw';

$route['genome/cancel'] = 'genome/genome_controller/cancel';
$route['genome/decline'] = 'genome/genome_controller/decline';
$route['genome/success'] = 'genome/genome_controller/success';


/*perfect money */
$route['perfectmoney/deposit'] = 'genome/perfectmoney_controller/deposit';
$route['perfectmoney/withdraw'] = 'genome/perfectmoney_controller/withdraw';

$route['perfectmoney/cancel'] = 'genome/perfectmoney_controller/cancel';
$route['perfectmoney/decline'] = 'genome/perfectmoney_controller/decline';
$route['perfectmoney/success'] = 'genome/perfectmoney_controller/success';

/*api router*/
$route['api'] = 'webservice/api/apichild/default'; 
$route['api/genome/deposit'] = 'webservice/api/apichild/genomedeposit'; 

/*crypto payments */
$route['crypto/deposit'] = 'genome/crypto_controller/deposit';
$route['crypto/withdraw'] = 'genome/crypto_controller/withdraw';

/*dohone payments */
$route['dohone/deposit'] = 'genome/dohone_controller/deposit';
$route['dohone/withdraw'] = 'genome/dohone_controller/withdraw';
